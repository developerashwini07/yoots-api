const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Organization = db.organization;
const Teacher = db.teacher;
const Group = db.group;
const Vendor = db.vendor;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const nodeBase64 = require('nodejs-base64-converter');
const validator = require('validator');

exports.signup = (req, res) => {
  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles },
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map((role) => role._id);
          user.save((err) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            res.send({ message: "User was registered successfully!" });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save((err) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          res.send({ message: "User was registered successfully!" });
        });
      });
    }
  });
};

exports.signin = (req, res) => {
  User.findOne({email: req.body.email,}).populate("roles", "-__v").exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (!user) {
      return res.status(404).send({ message: "User Not found." });
    }

    var passwordIsValid = bcrypt.compareSync(
      req.body.password,
      user.password
    );

    if (!passwordIsValid) {
      return res.status(401).send({ message: "Invalid Password!" });
    }

      const token = jwt.sign({ id: user.id },
                                config.secret, 
                              {
                                algorithm: 'HS256',
                                allowInsecureKeySizes: true,
                                expiresIn: 86400, // 24 hours
                              });

      var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }

      req.session.token = token;

      if(user.userType != undefined){
        if(user.userType == 'principal'){
          // Organization section
          Organization.findOne({userId:user._id,status:1}, function (err, result) {
            if (result) {
              const query_res = {
                groupid : result.groupid,
                organizationid : result.id,
                result
              };    

              res.status(200).send({
                  id: user._id,
                  username: user.username,
                  email: user.email,
                  roles: authorities,
                  token : token,
                  sub_user : query_res,
              });
            } else {
              return res.status(400).send({ message: "User Not found." });
            }
            // if (err) throw err;
          });
        }else if(user.userType == 'teacher'){
          // Teacher section
          Teacher.findOne({userId:user._id ,status:1}, function (err, result) {
            if (result){
              const query_res = {
                groupid : result.groupid,
                organizationid : result.organizationid,
                teacherid:result._id,
                result
              };  

              res.status(200).send({
                id: user._id,
                username: user.username,
                email: user.email,
                roles: authorities,
                token : token,
                sub_user : query_res,
            })
          }else {
            return res.status(400).send({ message: "User Not found." });
          }
          });

        }else if(user.userType == 'superuser'){
          // Group section
          Group.findOne({userId:user._id,status:1}, function (err, result) {
            if (result) {
              const query_res = { 
                groupid : result.id,
                result
              }; 
              res.status(200).send({
                id: user._id,
                username: user.username,
                email: user.email,
                roles: authorities,
                token : token,
                sub_user : query_res,
              });
            }else {
              return res.status(400).send({ message: "User Not found." });
            }
          });

        }else if(user.userType == 'user'){
          // Vender section
          Vendor.findOne({userId:user._id,status:1}, function (err, result) {
            if (result) {
              const query_res = { 
                vendorid : result.id,
                result
              }; 
              res.status(200).send({
                id: user._id,
                username: user.username,
                email: user.email,
                roles: authorities,
                token : token,
                sub_user : query_res,
              });
            }else {
              return res.status(400).send({ message: "User Not found." });
            }
          });
          
        }else{
          res.status(200).send({
            id: user._id,
            username: user.username,
            email: user.email,
            roles: authorities,
            token : token,
            user
          });
        }
      }else{
        res.status(200).send({
          id: user._id,
          username: user.username,
          email: user.email,
          roles: authorities,
          token : token,
          user
        });
      }
    });
};

exports.signout = async (req, res) => {
  try {
    req.session = null;
    return res.status(200).send({ message: "You've been signed out!" });
  } catch (err) {
    this.next(err);
  }
};

// Delete all Group from the database.
exports.deleteAll = (req, res) => {
  User.deleteMany({})
  .then((data) => {
      res.send({ message: `${data.deletedCount} User were deleted successfully!`,});
  })
  .catch((err) => {
      res.status(500).send({ message: err.message || "Some error occurred while removing all Users.",
      });
  });
};

exports.setPassword = (req, res) => {

  if (!req.body) {
    return res.status(400).send({
        message: "Data to update can not be empty!",
    });
  }

  if(validator.isEmpty(req.body.token)){
    res.status(400).send({ message: 'Token is required'});
    return true;
  }

  if(validator.isEmpty(req.body.password)){
    res.status(400).send({ message: 'Password is required'});
    return true;
  }

  // if(validator.isEmpty(req.body.confirm_password)){
  //   res.status(400).send({ message: 'Confirm Password is required'});
  //   return true;
  // }

  // if(req.body.password != req.body.confirm_password){
  //   res.status(400).send({ message: 'Confirm Password did not match'});
  //   return true;
  // }

  User.findOne({token: req.body.token}).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    /*
    if (user) {
      user.password = bcrypt.hashSync(req.body.password, 8);
      user.save((err) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }else{
          res.send({ message: "Password set successfully!" });
          return;
        }
      });
    }else{
      res.status(400).send({ message: 'user not found ' });
      return;
    } */

    if (user) {
      User.findByIdAndUpdate(user.id, {password: bcrypt.hashSync(req.body.password, 8)} , { useFindAndModify: false }).then((data) => {
          if (!data) {
              res.status(404).send({ message: `Cannot update password!`,'error':err});
          } else {
              res.send({ message: "Password set successfully!" });
          }
      })
      .catch((err) => {
          res.status(500).send({message: "Error updating password ",'error':err});
      });

    }else{
      res.status(400).send({ message: 'user not found ' });
      return;
    } 
  });
};


exports.updatePassword = async (req, res) => {
  const userId = req.userId; // Assuming the user ID is set in the request by the auth middleware
  const { currentPassword, newPassword } = req.body;

  try {
    const user = await User.findById(userId).exec();

    if (!user) {
      return res.status(404).send({ message: "User not found." });
    }

    const passwordIsValid = bcrypt.compareSync(currentPassword, user.password);

    if (!passwordIsValid) {
      return res.status(401).send({ message: "Invalid current password!" });
    }

    const newHashedPassword = bcrypt.hashSync(newPassword, 8);
    user.password = newHashedPassword;

    await user.save();

    return res.status(200).send({ message: "Password updated successfully!" });
  } catch (err) {
    return res.status(500).send({ message: err.message });
  }
};