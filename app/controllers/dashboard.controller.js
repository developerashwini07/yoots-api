const config = require("../config/auth.config");
const db = require("../models");
const Organization = db.organization;
const Teacher = db.teacher;
const Group = db.group;
const Vendor = db.vendor;
const Trip = db.trip;
const Quotation = db.quotation;
const mongoose = require("mongoose");


exports.yoot = async (req, res) => {

    const today = new Date().toISOString().slice(0, 10);
    let formatedDate = new Date(today).toISOString();
    let formatedDateRes = formatedDate.replace("Z", "+00:00");
    const condtiotnToday = formatedDateRes;

    /*** Total Group */
    const groupCondition = { status: 1 };
    let countGroup = await Group.countDocuments(groupCondition);

    /*** Total Organization */
    const orgCondition = { status: 1 };
    let countOrg = await Organization.countDocuments(orgCondition);

    /*** Total Teacher */
    const teacherCondition = { status: 1 };
    let countTeacher = await Teacher.countDocuments(teacherCondition);

    /*** Total Vendor */
    const vendorCondition = { status: 1 };
    let countVendor = await Vendor.countDocuments(vendorCondition);

    /*** Total Future/Upcoming Trip */
    const tripCondition = { status: 1, pickupDate: { $gte: condtiotnToday } };
    let countFutureTrip = await Trip.countDocuments(tripCondition);

    /*** Total Requested/Waiting for Quotation Trip */
    const tripCondition2 = { status: 1, tripstatus: 1 };
    let countWaitingQuotationTrip = await Trip.countDocuments(tripCondition2);

    /*** Total Quotation Selected Trip */
    const tripCondition3 = { status: 1, tripstatus: 2 };
    let countSubmitQuotationTrip = await Trip.countDocuments(tripCondition3);

    /*** Total Today Trip */
    const tripCondition4 = { status: 1, pickupDate: condtiotnToday };
    let countTodayTrip = await Trip.countDocuments(tripCondition4);

    let tripList1 = await Trip.aggregate([
        { $match: { ...tripCondition2 } },
        {
            $lookup: {
                from: "quotations",
                localField: "_id",
                foreignField: "trip",
                as: "quotations",
            },
        },

    ]);

    let quatitonReceivedCount = 0;
    tripList1.forEach((element) => {
        if ((element.quotations) && (element.quotations.length > 0)) {
            quatitonReceivedCount += 1;
        }
    });

    res.status(200).send({
        totalGroup: countGroup,
        totalOrg: countOrg,
        totalTeacher: countTeacher,
        totalVendor: countVendor,
        totalUpcomingTrip: countFutureTrip,
        totalWaitingQutation: countWaitingQuotationTrip,
        totalQutationSubmit: countSubmitQuotationTrip,
        totalTodayTrip: countTodayTrip,
        totalWaitingForConfirmationQuote: quatitonReceivedCount,
        date: condtiotnToday
    });

};

exports.group = async (req, res) => {
    const { groupid } = req.query;
    if (groupid == 'undefined' || groupid == undefined) {
        res.status(400).send({ message: 'Group Id is required' });
        return true;
    }

    const today = new Date().toISOString().slice(0, 10);
    let formatedDate = new Date(today).toISOString();
    let formatedDateRes = formatedDate.replace("Z", "+00:00");
    const condtiotnToday = formatedDateRes;

    /*** Total Organization */
    const orgCondition = { status: 1, groupid: groupid }
    let countOrg = await Organization.countDocuments(orgCondition);

    /*** Total Teacher */
    const teacherCondition = { status: 1, groupid: groupid }
    let countTeacher = await Teacher.countDocuments(teacherCondition);

    /*** Total Future/Upcoming Trip */
    const tripCondition = { status: 1, pickupDate: { $gte: condtiotnToday }, groupid: groupid }
    let countFutureTrip = await Trip.countDocuments(tripCondition);

    /*** Total Requested/wating for qutation Trip */
    const tripCondition2 = { status: 1, tripstatus: 1, groupid: groupid }
    let countwaitingQutationTrip = await Trip.countDocuments(tripCondition2);

    /*** Total qutation selected Trip */
    const tripCondition3 = { status: 1, tripstatus: 2, groupid: groupid }
    let countsubmitQutationTrip = await Trip.countDocuments(tripCondition3);

    /*** Total Today Trip */
    const tripCondition4 = { status: 1, pickupDate: condtiotnToday, groupid: groupid }
    let countTodayTrip = await Trip.countDocuments(tripCondition4);

    let tripList1 = await Trip.aggregate([
        { $match: { ...tripCondition2 } },
        {
            $lookup: {
                from: "quotations",
                localField: "_id",
                foreignField: "trip",
                as: "quotations",
            },
        },

    ]);

    let quatitonReceivedCount = 0;
    tripList1.forEach((element) => {
        if ((element.quotations) && (element.quotations.length > 0)) {
            quatitonReceivedCount += 1;
        }
    });

    res.status(200).send({
        totalOrg: countOrg,
        totalTeacher: countTeacher,
        totalUpcomingTrip: countFutureTrip,
        totalWaitingQutation: countwaitingQutationTrip,
        totalQutationSubmit: countsubmitQutationTrip,
        totalTodayTrip: countTodayTrip,
        totalWaitingForConfirmationQuote: quatitonReceivedCount,
        date: condtiotnToday
    });

};


exports.organization = async (req, res) => {
    const { organizationid } = req.query;
    if (organizationid == "undefined" || organizationid == undefined) {
        res.status(400).send({ message: "Organization Id is required" });
        return true;
    }

    const today = new Date().toISOString().slice(0, 10);
    let formatedDate = new Date(today).toISOString();
    let formatedDateRes = formatedDate.replace("Z", "+00:00");
    const condtiotnToday = formatedDateRes;

    /*** Total Teacher */
    const teacherCondition = { status: 1, organizationid: organizationid };
    let countTeacher = await Teacher.countDocuments(teacherCondition);

    /*** Total Future/Upcoming Trip */
    const tripCondition = {
        status: 1,
        pickupDate: { $gte: condtiotnToday },
        organizationid: organizationid,
    };
    let countFutureTrip = await Trip.countDocuments(tripCondition);

    /*** Total Requested/wating for qutation Trip */
    const tripCondition2 = {
        status: 1,
        tripstatus: 1,
        organizationid: organizationid,
    };
    let countwaitingQutationTrip = await Trip.countDocuments(tripCondition2);

    /*** Total qutation selected Trip */
    const tripCondition3 = {
        status: 1,
        tripstatus: 2,
        organizationid: organizationid,
    };
    let countsubmitQutationTrip = await Trip.countDocuments(tripCondition3);

    /*** Total Today Trip */
    const tripCondition4 = {
        status: 1,
        pickupDate: condtiotnToday,
        organizationid: organizationid,
    };
    let countTodayTrip = await Trip.countDocuments(tripCondition4);
    let tripList1 = await Trip.aggregate([
        { $match: { ...tripCondition2 } },
        {
            $lookup: {
                from: "quotations",
                localField: "_id",
                foreignField: "trip",
                as: "quotations",
            },
        },
    ]);

    let quatitonReceivedCount = 0;
    tripList1.forEach((element) => {
        if (element.quotations && element.quotations.length > 0) {
            quatitonReceivedCount += 1;
        }
    });

    // Step 2: Calculate the start and end dates of the current week
    const today1 = new Date();
    const dayOfWeek = today1.getDay(); // 0 (Sunday) to 6 (Saturday)
    const startOfWeek = new Date(today1.setDate(today1.getDate() - dayOfWeek));
    startOfWeek.setHours(0, 0, 0, 0); // Set to the start of the day

    const endOfWeek = new Date(startOfWeek);
    endOfWeek.setDate(startOfWeek.getDate() + 6);
    endOfWeek.setHours(23, 59, 59, 999); // Set to the end of the day

    const tripCondition5 = {
        status: 1,
        pickupDate: { $gte: startOfWeek, $lte: endOfWeek },
        organizationid: organizationid,
    };

    let countTripsThisWeek = await Trip.countDocuments(tripCondition5);
    res.status(200).send({
        totalTeacher: countTeacher,
        totalUpcomingTrip: countFutureTrip,
        totalWaitingQutation: countwaitingQutationTrip,
        totalQutationSubmit: countsubmitQutationTrip,
        totalTodayTrip: countTodayTrip,
        totalWaitingForConfirmationQuote: quatitonReceivedCount,
        totalTripsThisWeek: countTripsThisWeek,
        date: condtiotnToday,
    });
};

exports.teacher = async (req, res) => {
    const { teacherid } = req.query;
    if (teacherid == 'undefined' || teacherid == undefined) {
        res.status(400).send({ message: 'Teacher Id is required' });
        return true;
    }

    const today = new Date().toISOString().slice(0, 10);
    let formatedDate = new Date(today).toISOString();
    let formatedDateRes = formatedDate.replace("Z", "+00:00");
    const condtiotnToday = formatedDateRes;

    /*** Total Future/Upcoming Trip */
    const tripCondition = { status: 1, pickupDate: { $gte: condtiotnToday }, teacherid: teacherid }
    let countFutureTrip = await Trip.countDocuments(tripCondition);

    /*** Total Requested/wating for qutation Trip */
    const tripCondition2 = { status: 1, tripstatus: 1, teacherid: teacherid }
    let countwaitingQutationTrip = await Trip.countDocuments(tripCondition2);

    /*** Total qutation selected Trip */
    const tripCondition3 = { status: 1, tripstatus: 2, teacherid: teacherid }
    let countsubmitQutationTrip = await Trip.countDocuments(tripCondition3);

    /*** Total Today Trip */
    const tripCondition4 = { status: 1, pickupDate: condtiotnToday, teacherid: teacherid }
    let countTodayTrip = await Trip.countDocuments(tripCondition4);

    let tripList1 = await Trip.aggregate([
        { $match: { ...tripCondition2 } },
        {
            $lookup: {
                from: "quotations",
                localField: "_id",
                foreignField: "trip",
                as: "quotations",
            },
        },

    ]);

    let quatitonReceivedCount = 0;
    tripList1.forEach((element) => {
        if ((element.quotations) && (element.quotations.length > 0)) {
            quatitonReceivedCount += 1;
        }
    });


    res.status(200).send({
        totalUpcomingTrip: countFutureTrip,
        totalWaitingQutation: countwaitingQutationTrip,
        totalQutationSubmit: countsubmitQutationTrip,
        totalTodayTrip: countTodayTrip,
        totalWaitingForConfirmationQuote: quatitonReceivedCount,
        date: condtiotnToday
    });

};

exports.vendor = async (req, res) => {
    const { vendorid } = req.query;
    if (vendorid == "undefined" || vendorid == undefined) {
        res.status(400).send({ message: "Vendor Id is required" });
        return true;
    }

    const today = new Date().toISOString().slice(0, 10);
    let formatedDate = new Date(today).toISOString();
    let formatedDateRes = formatedDate.replace("Z", "+00:00");
    const condtiotnToday = formatedDateRes;

    /*** Total Requested/wating for qutation Trip */
    const tripCondition2 = { status: 1, tripstatus: 1 };
    let countwaitingQutationTrip = await Trip.countDocuments(tripCondition2);

    /*** Total Subitted qutation Trip */
    const quoteCondition2 = { status: 1, quotationBy: vendorid };
    let countsubmittedQutation = await Quotation.countDocuments(quoteCondition2);

    /*** Total Selected qutation Trip */
    const quoteCondition3 = {
        status: 1,
        quotationBy: vendorid,
        quotationstatus: 1,
    };

    let countselectedQutation = await Quotation.countDocuments(quoteCondition3);

    let countTripsForVendor = await Quotation.aggregate([
        {
            $match: {
                status: 1,
                quotationBy: mongoose.Types.ObjectId(vendorid),
                quotationstatus: 1,
            },
        },
        {
            $lookup: {
                from: "trips", // Ensure this matches the actual name of your 'trips' collection
                localField: "trip",
                foreignField: "_id",
                as: "tripDetails",
            },
        },
        {
            $unwind: "$tripDetails",
        },
        {
            $match: {
                "tripDetails.status": 1,
                // 'tripDetails.driverDetails': { $exists: true, $ne: [] } // Ensures driverDetails exists and is not empty
            },
        },
        // {
        //   $count: "totalTrips",
        // },
        {
            $project: {
                tripDetails: 1,
              //  driverDetailsCount: { $size: "$tripDetails.driverDetails" }, // Count the length of the driverDetails array
            },
        },
    ]);
    let quatitonReceivedCount = 0;
    await countTripsForVendor.map((element) => {
        if (element.tripDetails && element.tripDetails.driverDetails.length > 0) {
            quatitonReceivedCount += 1;
            console.log(
                quatitonReceivedCount,
                "quatitonReceivedCountquatitonReceivedCount"
            );
        } else {
            quatitonReceivedCount += 1;
        }
    });

    /*** Total Future/Upcoming Trip */
    const tripCondition = {
        status: 1,
        pickupDate: { $gte: condtiotnToday }
      };
      
      const countFutureTrip = await Trip.aggregate([
        {
          $match: tripCondition
        },
        {
          $count: "totalTrips"
        }
      ]);
      
      const status2Trips = await Trip.aggregate([
        {
          $match: {
            // Other conditions...
            status: 1,
            pickupDate: { $gte: new Date(condtiotnToday) },
            tripStatus: { $ne: 0 },
          },
        },
        {
          $lookup: {
            from: "quotations",
            localField: "_id",
            foreignField: "trip",
            as: "quotations",
          },
        },
        { $unwind: { path: "$quotations", preserveNullAndEmptyArrays: true } },
        {
          $match: {
            $and: [
              {
                $and: [
                  {
                    tripstatus: { $in: [1, 2, 3, 6] },
                  },
                ],
              },
              {
                $or: [
                  {
                    "quotations.quotationBy": mongoose.Types.ObjectId(vendorid),
                  },
                  { "quotations.quotationstatus": { $in: [1] } },
                  {
                    tripstatus: { $in: [2, 3, 6] },
                  },
                  { quotations: { $eq: null } },
                ],
              },
              {
                $or: [{ "quotations.status": 1 }, { quotations: { $eq: null } }],
              },
              {
                $or: [
                  {
                    $and: [
                      {
                        "quotations.quotationBy": mongoose.Types.ObjectId(vendorid),
                      },
                      { "quotations.quotationstatus": { $in: [1] } },
                      {
                        tripstatus: { $in: [2, 3, 6, 1] },
                      },
                    ],
                  },
                  { quotations: { $eq: null } },
                ],
              },
            ],
          },
        },
        {
          $group: {
            _id: "$_id",
            tripDetails: { $first: "$$ROOT" },
            quotations: { $push: "$quotations" },
          },
        },
        {
          $addFields: {
            "tripDetails.quotations": {
              $cond: {
                if: { $eq: ["$quotations", [{}]] },
                then: [],
                else: "$quotations",
              },
            },
          },
        },
        {
          $replaceRoot: {
            newRoot: "$tripDetails",
          },
        },
        {
          $facet: {
            totalDocs: [{ $count: "count" }],
          },
        },
        {
          $project: {
            totalDocs: { $ifNull: [{ $arrayElemAt: ["$totalDocs.count", 0] }, 0] },
            docs: 1,
          },
        },
      ]);
     

    const quotationCondition = {
        status: 1,
        quotationBy: vendorid,
        quotationstatus: 1,
        reQuoteRequired: 0,
    };

    let countApprovedQuotations = await Quotation.countDocuments(
        quotationCondition
    );

    res.status(200).send({
        totalUpcomingTrip: status2Trips[0].totalDocs,
        totalWaitingQutation: countwaitingQutationTrip,
        totalsubmittedQutation: countsubmittedQutation,
        totalApprovedQuotations: countApprovedQuotations,
        totalselectedQutation: countselectedQutation,
        totalUnassignedTrip: quatitonReceivedCount,
        date: condtiotnToday,
    });
};

