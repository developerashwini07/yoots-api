const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Group = db.group;
const Teacher = db.teacher;
const Organization = db.organization;
const nodemailer = require('nodemailer');
const accountSid = "ACc65a0bfe268ebbd0a47bfbb87f5c940b";
const authToken  =  "a59e2b131fa36e3f13fe22f5e9d680eb";
const twilioClient = require('twilio')(accountSid,authToken);
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { getMaxListeners } = require("../models/user.model");
const sgMail = require('@sendgrid/mail');
const validator = require('validator');
const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;
    return { limit, offset };
};

exports.findAll = (req, res) => {
    const { page, size, groupname } = req.query;
    var condition = groupname ? { groupname: { $regex: new RegExp(groupname), $options: "i" }, status : 1 } : { status:1};
    const { limit, offset } = getPagination(page, size);
    Group.paginate(condition, { offset, limit ,sort:{ _id : -1} })
    .then((data) => {
        res.send({
        totalItems: data.totalDocs,
        list: data.docs,
        totalPages: data.totalPages,
        currentPage: data.page - 1,
        });
    })
    .catch((err) => {
        res.status(500).send({
        message:
            err.message || "Some error occurred while retrieving groups.",
        });
    });
};

exports.getAll = (req,res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    Group.paginate({ "status":1}, { offset, limit }).then((data) => {
        res.send({
            totalItems: data.totalDocs,
            data: data.docs,
            totalPages: data.totalPages,
            currentPage: data.page - 1,
        });
    })
    .catch((err) => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving groups.",
        });
    });
};


exports.register_old = (req, res) => {

    if(req.body.groupname == undefined){
        res.status(400).send({ message: 'Group name is required'});
        return;
    }

    if(validator.isEmpty(req.body.groupname)){
        res.status(400).send({ message: 'Group name is required'});
        return;
    }

    if(req.body.groupadmin == undefined){
        res.status(400).send({ message: 'Group admin name is required'});
        return;
    }
    
    if(validator.isEmpty(req.body.groupadmin)){
        res.status(400).send({ message: 'Group admin name is required'});
        return;
    }

    if(req.body.email == undefined){
        res.status(400).send({ message: 'Group email is required'});
        return;
    }

    if(validator.isEmpty(req.body.email)){
        res.status(400).send({ message: 'Group email is required'});
        return;
    }

    if(!validator.isEmail(req.body.email)){
        res.status(400).send({ message: 'Please enter valid email'});
        return;
    }

    if(req.body.phone == undefined){
        res.status(400).send({ message: 'Phone number is required'});
        return;
    }

    if(validator.isEmpty(req.body.phone)){
        res.status(400).send({ message: 'Phone number is required'});
        return;
    }

    if(!validator.isNumeric(req.body.phone)){
        res.status(400).send({ message: 'only number allowed'});
        return;
    }

    if(req.body.phone.length < 9 ){
        res.status(400).send({ message: 'minimum 9 character allowed'});
        return;
    }

    if(req.body.phone.length > 15 ){
        res.status(400).send({ message: 'maximum 15 character allowed'});
        return;
    }

    if(req.body.address == undefined){
        res.status(400).send({ message: 'Addess is required'});
        return;
    }

    if(validator.isEmpty(req.body.address)){
        res.status(400).send({ message: 'Addess is required'});
        return;
    }

    const group = new Group({
        groupname: req.body.groupname,
        groupadmin: req.body.groupadmin,
        email: req.body.email,
        phone:req.body.phone,
        password: bcrypt.hashSync(req.body.password, 8),
        address: req.body.address
    });

    group.save((err, group) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }
        const user = new User({
            username: req.body.groupadmin,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 8),
        });

        user.save((err, user) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }
            Role.findOne({ name: "superuser" }, (err, role) => {
                if (err) {
                res.status(500).send({ message: err });
                return;
                }
                user.roles = [role._id];
                user.save((err) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }

                    /*
                    var transport = nodemailer.createTransport({
                        host: "sandbox.smtp.mailtrap.io",
                        port: 2525,
                        auth: {
                            user: "cc3626a7b30b5f",
                            pass: "5d749be6c616c1"
                        }
                    });
                    const mailOptions = {
                        from: 'ashwiniwebdeveloper@gmail.com',
                        to: req.body.email,
                        subject: 'Register Successsfully',
                        text: 'Group Create Successfully '+ '<br> Your UseName '+ req.body.groupadmin + ' and password is '+ req.body.password,
                    };
                    // Send the email
                    transport.sendMail(mailOptions, function(error, info){
                        if (error) {
                            console.log('Error:', error);
                        } else {
                            console.log('Email sent:', info.response);
                        }
                    });  */

                    var transport = nodemailer.createTransport({
                        host: "email-smtp.us-east-2.amazonaws.com",
                        port: 587,
                        secure : false,
                        auth: {
                            user: "AKIAZQ3DO55AI53CEWXU",
                            pass: "BOLTt3FdvQIsDLFcbjR0fdMnWS0v+WsytXQyVM5tmN17"
                        }
                    });
                
                    const mailOptions = {
                        from: 'glory311292@gmail.com',
                        to: req.body.email,
                        //to : 'developerashwini07@gmail.com',
                        subject: 'Register Successsfully',
                        html: 'Group Create Successfully <br> Your UserName <strong>'+ req.body.groupadmin + '</strong> and password is <strong>'+ req.body.password + '</strong> and registered email is <strong>' + req.body.email + '</strong> ',
                    };
                
                    // Send the email
                    transport.sendMail(mailOptions, function(error, info){
                        if (error) {
                            console.log('Error:', error);
                        } else {
                            //console.log('Email sent:', info.response);
                            res.send({ message: "Code sent successfully!" });
                        }
                    });
        
                    res.send({ message: "Group registered successfully!" });
                });
            });
        });
    });
};

    

exports.register = (req, res) => {

    if(req.body.groupname == undefined){
        res.status(400).send({ message: 'Group name is required'});
        return;
    }

    if(validator.isEmpty(req.body.groupname)){
        res.status(400).send({ message: 'Group name is required'});
        return;
    }

    if(req.body.groupadmin == undefined){
        res.status(400).send({ message: 'Group admin name is required'});
        return;
    }
    
    if(validator.isEmpty(req.body.groupadmin)){
        res.status(400).send({ message: 'Group admin name is required'});
        return;
    }

    if(req.body.email == undefined){
        res.status(400).send({ message: 'Group email is required'});
        return;
    }

    if(validator.isEmpty(req.body.email)){
        res.status(400).send({ message: 'Group email is required'});
        return;
    }

    if(!validator.isEmail(req.body.email)){
        res.status(400).send({ message: 'Please enter valid email'});
        return;
    }

    if(req.body.phone == undefined){
        res.status(400).send({ message: 'Phone number is required'});
        return;
    }

    if(validator.isEmpty(req.body.phone)){
        res.status(400).send({ message: 'Phone number is required'});
        return;
    }

    if(!validator.isNumeric(req.body.phone)){
        res.status(400).send({ message: 'only number allowed'});
        return;
    }

    if(req.body.phone.length < 9 ){
        res.status(400).send({ message: 'minimum 9 character allowed'});
        return;
    }

    if(req.body.phone.length > 15 ){
        res.status(400).send({ message: 'maximum 15 character allowed'});
        return;
    }

    if(req.body.address == undefined){
        res.status(400).send({ message: 'Addess is required'});
        return;
    }

    if(validator.isEmpty(req.body.address)){
        res.status(400).send({ message: 'Addess is required'});
        return;
    }

    Group.countDocuments({}, function(err, count) {
        if (err) throw err;
        const newId = 'GA00'+ (count + 1);
        const group = new Group({
            groupid: newId,
            groupname: req.body.groupname,
            groupadmin: req.body.groupadmin,
            email: req.body.email,
            phone:req.body.phone,
            password: bcrypt.hashSync(req.body.groupadmin, 8),
            address: req.body.address,
            status :1
        });

        group.save((err, group) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }
            const user = new User({
                username: req.body.groupadmin,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.groupadmin, 8),
                //token : nodeBase64.encode("req.body.email"),
            });

            user.save((err, user) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }
                Role.findOne({ name: "superuser" }, (err, role) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }
                    user.roles = [role._id];
                    user.userType = "superuser";
                    user.token = user._id
                    user.save((err) => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }

                        //Email to user
                        sgMail.setApiKey(process.env.SENDGRID_KEY);
                        const password_token  = user._id;
                        const message = {
                            to: {
                                email: req.body.email,
                                name: req.body.groupadmin
                            },
                            from: {
                                email: process.env.SENDGRID_FROM_EMAIL,
                                name: process.env.SENDGRID_FROM_NAME
                            },
                            "dynamic_template_data":{
                                'user_name' : req.body.groupadmin,
                                'user_email' : req.body.email,
                                'password_link' : process.env.FRONEND_URL+'/set-password?token=' + password_token,
                            },
                            template_id:process.env.MEMBER_EMAIL_TEMPLATE_ID
                        };

                        sgMail.send(message).then(() => {})
                        .catch(error => {
                            console.error(error);
                        });
                        group.userId =  user._id;
                        group.save((err) => {
                            if (err) {
                                res.status(500).send({ message: err });
                                return;
                            }
                            res.send({ message: "Group registered successfully!" });
                        });
                    });
                });
            });
        });
    });
};


// Find a single Group with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Group.findById(id)
    .then((data) => {
        if (!data){
            res.status(400).send({ message: "Not found Group with id " + id });
        } else { 
            res.send({'data':data});
        }
    })
    .catch((err) => {
        res.status(500).send({ message: "Error retrieving Group with id=" + id });
    });
};

// Update a Group by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!",
        });
    }

    if(req.body.groupname == undefined){
        res.status(400).send({ message: 'Group name is required'});
        return;
    }

    if(validator.isEmpty(req.body.groupname)){
        res.status(400).send({ message: 'Group name is required'});
        return;
    }

    if(req.body.groupadmin == undefined){
        res.status(400).send({ message: 'Group admin name is required'});
        return;
    }
    
    if(validator.isEmpty(req.body.groupadmin)){
        res.status(400).send({ message: 'Group admin name is required'});
        return;
    }

    if(req.body.email == undefined){
        res.status(400).send({ message: 'Group email is required'});
        return;
    }

    if(validator.isEmpty(req.body.email)){
        res.status(400).send({ message: 'Group email is required'});
        return;
    }

    if(!validator.isEmail(req.body.email)){
        res.status(400).send({ message: 'Please enter valid email'});
        return;
    }

    if(req.body.phone == undefined){
        res.status(400).send({ message: 'Phone number is required'});
        return;
    }

    if(validator.isEmpty(req.body.phone)){
        res.status(400).send({ message: 'Phone number is required'});
        return;
    }

    if(!validator.isNumeric(req.body.phone)){
        res.status(400).send({ message: 'only number allowed'});
        return;
    }

    if(req.body.phone.length < 9 ){
        res.status(400).send({ message: 'minimum 9 character allowed'});
        return;
    }

    if(req.body.phone.length > 15 ){
        res.status(400).send({ message: 'maximum 15 character allowed'});
        return;
    }

    if(req.body.address == undefined){
        res.status(400).send({ message: 'Addess is required'});
        return;
    }

    if(validator.isEmpty(req.body.address)){
        res.status(400).send({ message: 'Addess is required'});
        return;
    }

    const id = req.body.id;
    
    //Update user collection email if requested email is different from existing record
    Group.findById(id).then((data) => {
        if (!data){
            res.status(400).send({ message: "Not found Group with id " + id });
        } else { 
            if(data.email != req.body.email){
                User.findByIdAndUpdate(data.userId, {'email': req.body.email}, { useFindAndModify: false })
                .then((data) => {
                    if (!data) {
                        res.status(404).send({ message: `Cannot update user email`,'error':err});
                    }
                });
            }
        }
    }).catch((err) => {
        res.status(500).send({message: "Error updating Group with id=" + id,'error':err});
    });


    const updateRecord = {
        'groupname': req.body.groupname,
        'groupadmin': req.body.groupadmin,
        'email': req.body.email,
        'phone':req.body.phone,
        'address': req.body.address
    };

    Group.findByIdAndUpdate(id, updateRecord , { useFindAndModify: false }).then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot update Group with id= ${id}. Maybe Group was not found!`,'error':err});
        } else {
            res.send({ message: "Group was updated successfully." });
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Error updating Group with id=" + id,'error':err});
    });
};


//Soft delete with the specified id in the request
exports.removeData = (req, res) => {
    const id = req.body.id;
    const updateRecord = {'status': 0}
    Group.findByIdAndUpdate(id, updateRecord, { useFindAndModify: false })
    .then(async(data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot delete Group with id= ${id}. Maybe Group was not found!`,'error':err});
        } else {
             // Update the groupid field in the Teacher model
            const updateTeachers = await Teacher.updateMany({ groupid: id }, { $set: { groupid: null } });
            const updateOrganization = await Organization.updateMany({ groupid: id }, { $set: { groupid: null } });
            // if (!updateTeachers.nModified) {
            //     return res.status(404).send({ message: `No teachers found with groupid= ${id}.` });
            // }
            res.send({ message: "Group was deleted successfully." });
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Error deleting Group with id=" + id,'error':err});
    });
};
  
// Delete a Group with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Group.findByIdAndRemove(id, { useFindAndModify: false }).then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot delete Group with id=${id}. Maybe Group was not found!`,
        });
        } else {
            res.send({ message: "Group was deleted successfully!",});
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Could not delete Group with id=" + id,'error':err });
    });
};
  
// Delete all Group from the database.
exports.deleteAll = (req, res) => {
    Group.deleteMany({}).then((data) => {
        res.send({ message: `${data.deletedCount} Group were deleted successfully!`,});
    })
    .catch((err) => {
        res.status(500).send({ message: err.message || "Some error occurred while removing all Groups.",
        });
    });
};