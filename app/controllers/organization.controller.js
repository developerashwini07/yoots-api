const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Organization = db.organization;
const Group = db.group;
const Teacher = db.teacher;
const nodemailer = require('nodemailer');
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { getMaxListeners } = require("../models/user.model");
const sgMail = require('@sendgrid/mail');
const validator = require('validator');
const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;
    return { limit, offset };
};

exports.findAll = (req, res) => {
    const { page, size, groupid} = req.query;
    if(groupid == 'undefined' || groupid == undefined){
        res.status(400).send({ message: 'Group Id is required'});
        return true;
    }
    var condition =  { groupid :  groupid , status:1};
    const { limit, offset } = getPagination(page, size);
    Organization.paginate(condition, { offset, limit,sort:{ _id : -1} }).then((data) => {
        res.send({
            totalItems: data.totalDocs,
            list: data.docs,
            totalPages: data.totalPages,
            currentPage: data.page - 1,
        });
    })
    .catch((err) => {
        res.status(500).send({
        message:
            err.message || "Some error occurred while retrieving Organization.",
        });
    });
};

exports.getAll = (req,res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    Organization.paginate({ "status":1}, { offset, limit })
        .then((data) => {
        res.send({
            totalItems: data.totalDocs,
            data: data.docs,
            totalPages: data.totalPages,
            currentPage: data.page - 1,
        });
        })
        .catch((err) => {
        res.status(500).send({
            message:
            err.message || "Some error occurred while retrieving Organization.",
        });
    });
};  

exports.register = (req, res) => {

    if(req.body.groupid == undefined){
        res.status(400).send({ message: 'Group ID is required'});
        return true;
    }

    if(validator.isEmpty(req.body.groupid)){
        res.status(400).send({ message: 'Group ID is required'});
        return true;
    }
    
    if(req.body.organizationname == undefined){
        res.status(400).send({ message: 'Organisation name is required'});
        return true;
    }

    if(validator.isEmpty(req.body.organizationname)){
        res.status(400).send({ message: 'Organisation name is required'});
        return true;
    }

    if(req.body.organizationadmin == undefined){
        res.status(400).send({ message: 'Contact person name is required'});
        return true;
    }

    if(validator.isEmpty(req.body.organizationadmin)){
        res.status(400).send({ message: 'Contact person name is required'});
        return true;
    }

    if(req.body.email == undefined){
        res.status(400).send({ message: 'Organisation email is required'});
        return true;
    }

    if(validator.isEmpty(req.body.email)){
        res.status(400).send({ message: 'Organisation email is required'});
        return true;
    }

    if(!validator.isEmail(req.body.email)){
        res.status(400).send({ message: 'Enter valid email'});
        return true;
    }

    if(req.body.phone == undefined){
        res.status(400).send({ message: 'Phone number is required'});
        return true;
    }

    if(validator.isEmpty(req.body.phone)){
        res.status(400).send({ message: 'Phone number is required'});
        return true;
    }

    if(!validator.isNumeric(req.body.phone)){
        res.status(400).send({ message: 'only number allowed'});
        return true;
    }

    if(req.body.phone.length < 9 ){
        res.status(400).send({ message: 'minimum 9 character allowed'});
        return true;
    }

    if(req.body.phone.length > 15 ){
        res.status(400).send({ message: 'maximum 15 character allowed'});
        return true;
    }

    if(req.body.address == undefined){
        res.status(400).send({ message: 'Addess is required'});
        return true;
    }

    if(validator.isEmpty(req.body.address)){
        res.status(400).send({ message: 'Addess is required'});
        return true;
    }

    if(req.body.designation == undefined){
        res.status(400).send({ message: 'Designation is required'});
        return true;
    }

    if(validator.isEmpty(req.body.designation)){
        res.status(400).send({ message: 'Designation is required'});
        return true;
    }
 
    Organization.countDocuments({}, function(err, count) {
        if (err) throw err;
        const newId = 'ORG00'+ (count + 1);
        const organization = new Organization({
            organizationid :newId,
            groupid: req.body.groupid,
            organizationname: req.body.organizationname,
            organizationadmin: req.body.organizationadmin,
            email: req.body.email,
            phone:req.body.phone,
            password: bcrypt.hashSync(req.body.organizationadmin, 8),
            address: req.body.address,
            designation: req.body.designation,
            status :1
        });

        organization.save((err, organization) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }

            const user = new User({
                username: req.body.organizationadmin,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.organizationadmin, 8),
            });

            user.save((err, user) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }
                Role.findOne({ name: "principal" }, (err, role) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }
                    user.roles = [role._id];
                    user.userType = "principal";
                    user.token = user._id
                    user.save((err) => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }

                        //Email to user

                        sgMail.setApiKey(process.env.SENDGRID_KEY);
                        const password_token  = user._id;
                        const message = {
                            to: {
                                email: req.body.email,
                                name: req.body.organizationadmin
                            },
                            from: {
                                email: process.env.SENDGRID_FROM_EMAIL,
                                name: process.env.SENDGRID_FROM_NAME
                            },
                            "dynamic_template_data":{
                                'user_name' : req.body.organizationadmin,
                                'user_email' : req.body.email,
                                'password_link' : process.env.FRONEND_URL+'/set-password?token=' + password_token,
                            },
                            template_id:process.env.MEMBER_EMAIL_TEMPLATE_ID
                        };

                        sgMail.send(message).then(() => {})
                        .catch(error => {
                            console.error(error);
                        });
                        organization.userId =  user._id;
                        organization.save((err) => {
                            if (err) {
                                res.status(500).send({ message: err });
                                return;
                            }
                            res.send({ message: "Organisation registered successfully!" });
                        });
                    });
                });
            });
        });
    });
};


// Find a single Organization with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Organization.findById(id)
    .then((data) => {
        if (!data){
            res.status(400).send({ message: "Not found Organization with id " + id });
        } else { 
            res.send({'data':data});
        }
    })
    .catch((err) => {
        res.status(500).send({ message: "Error retrieving Organization with id=" + id });
    });
};

// Update a Organization by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!",
        });
    }

    if(req.body.groupid == undefined){
        res.status(400).send({ message: 'Group ID is required'});
        return true;
    }

    if(validator.isEmpty(req.body.groupid)){
        res.status(400).send({ message: 'Group ID is required'});
        return true;
    }
    
    if(req.body.organizationname == undefined){
        res.status(400).send({ message: 'Organisation name is required'});
        return true;
    }

    if(validator.isEmpty(req.body.organizationname)){
        res.status(400).send({ message: 'Organisation name is required'});
        return true;
    }

    if(req.body.organizationadmin == undefined){
        res.status(400).send({ message: 'Contact person name is required'});
        return true;
    }

    if(validator.isEmpty(req.body.organizationadmin)){
        res.status(400).send({ message: 'Contact person name is required'});
        return true;
    }

    if(req.body.email == undefined){
        res.status(400).send({ message: 'Organisation email is required'});
        return true;
    }

    if(validator.isEmpty(req.body.email)){
        res.status(400).send({ message: 'Organisation email is required'});
        return true;
    }

    if(!validator.isEmail(req.body.email)){
        res.status(400).send({ message: 'Enter valid email'});
        return true;
    }

    if(req.body.phone == undefined){
        res.status(400).send({ message: 'Phone number is required'});
        return true;
    }

    if(validator.isEmpty(req.body.phone)){
        res.status(400).send({ message: 'Phone number is required'});
        return true;
    }

    if(!validator.isNumeric(req.body.phone)){
        res.status(400).send({ message: 'only number allowed'});
        return true;
    }

    if(req.body.phone.length < 9 ){
        res.status(400).send({ message: 'minimum 9 character allowed'});
        return true;
    }

    if(req.body.phone.length > 15 ){
        res.status(400).send({ message: 'maximum 15 character allowed'});
        return true;
    }

    if(req.body.address == undefined){
        res.status(400).send({ message: 'Addess is required'});
        return true;
    }

    if(validator.isEmpty(req.body.address)){
        res.status(400).send({ message: 'Addess is required'});
        return true;
    }

    if(req.body.designation == undefined){
        res.status(400).send({ message: 'Designation is required'});
        return true;
    }

    if(validator.isEmpty(req.body.designation)){
        res.status(400).send({ message: 'Designation is required'});
        return true;
    }

    const id = req.body.id;
    
    //Update user collection email if requested email is different from existing record
    Organization.findById(id).then((data) => {
        if (!data){
            res.status(400).send({ message: "Not found Organization with id " + id });
        } else { 
            if(data.email != req.body.email){
                User.findByIdAndUpdate(data.userId, {'email': req.body.email}, { useFindAndModify: false })
                .then((data) => {
                    if (!data) {
                        res.status(404).send({ message: `Cannot update user email`,'error':err});
                    }
                });
            }
        }
    }).catch((err) => {
        res.status(500).send({message: "Error updating Organization with id=" + id,'error':err});
    });


    const updateRecord = {
        'groupid': req.body.groupid,
        'organizationname': req.body.organizationname,
        'organizationadmin': req.body.organizationadmin,
        'email': req.body.email,
        'phone':req.body.phone,
        'password': bcrypt.hashSync(req.body.organizationadmin, 8),
        'address': req.body.address,
        'designation': req.body.designation,
    };


    Organization.findByIdAndUpdate(id, updateRecord , { useFindAndModify: false })
    .then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot update Organization with id= ${id}. Maybe Group was not found!`,'error':err});
        } else {
            res.send({ message: "Organization was updated successfully." });
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Error updating Organization with id=" + id,'error':err});
    });
};


//Soft delete with the specified id in the request
exports.removeData = (req, res) => {
    const id = req.body.id;
    const updateRecord = {'status': 0}
    Organization.findByIdAndUpdate(id, updateRecord, { useFindAndModify: false })
    .then(async(data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot delete Organization with id= ${id}. Maybe Organization was not found!`,'error':err});
        } else {
            // Update the groupid field in the Teacher model
            const updateTeachers = await Teacher.updateMany({ organizationid: id }, { $set: { organizationid: null } });
           // if (!updateTeachers.nModified) {
           //     return res.status(404).send({ message: `No teachers found with groupid= ${id}.` });
           // }
            res.send({ message: "Organization was deleted successfully." });
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Error deleting Organization with id=" + id,'error':err});
    });
};
  
// Delete a Organization with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Organization.findByIdAndRemove(id, { useFindAndModify: false }).then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot delete Organization with id=${id}. Maybe Organization was not found!`,
        });
        } else {
            res.send({ message: "Organization was deleted successfully!",});
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Could not delete Organization with id=" + id,'error':err });
    });
};
  
// Delete all Organization from the database.
exports.deleteAll = (req, res) => {
    Organization.deleteMany({}).then((data) => {
        res.send({ message: `${data.deletedCount} Organization were deleted successfully!`,});
    })
    .catch((err) => {
            res.status(500).send({ message: err.message || "Some error occurred while removing all Organization.",
        });
    });
};

// Delete all Organization  related to specific Group Admin from the database.
exports.deleteAllByGroup = (req, res) => {
    const groupId  = req.body.id;
    Organization.deleteMany({groupid : groupId }).then((data) => {
        res.send({ message: `${data.deletedCount} Organization were deleted successfully!`,});
    })
    .catch((err) => {
        res.status(500).send({ message: err.message || "Some error occurred while removing all Organization.",
        });
    });
};

exports.getAllGroup = (req, res) => {
    Group.find({ "status":1}).sort({updatedAt : -1}).then((data) => {
        res.send({data: data});
    })
    .catch((err) => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving groups.",
        });
    });
};

