const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Teacher = db.teacher;
const Organization = db.organization;
const mongoose = require("mongoose");
const Group = db.group;
const nodemailer = require('nodemailer');
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { getMaxListeners } = require("../models/user.model");
const sgMail = require('@sendgrid/mail');
const validator = require('validator');
const getPagination = (page, size) => {
    const limit = size ? +size : 10;
    const offset = page ? page * limit : 0;
    return { limit, offset };
};

exports.findAll = (req, res) => {
    const { page, size, groupid ,organizationid} = req.query;
    if(groupid == 'undefined' || groupid == undefined){
        res.status(400).send({ message: 'Group Id is required'});
        return true;
    }
    if(organizationid == 'undefined' || organizationid == undefined){
        res.status(400).send({ message: 'Organization Id is required'});
        return true;
    }
    var condition =  { groupid : groupid ,organizationid : organizationid , status:1};
    const { limit, offset } = getPagination(page, size);
    Teacher.paginate(condition, { offset, limit,sort:{ _id : -1} }).then((data) => {
        res.send({
        totalItems: data.totalDocs,
        list: data.docs,
        totalPages: data.totalPages,
        currentPage: data.page - 1,
        });
    })
    .catch((err) => {
        res.status(500).send({
        message:
            err.message || "Some error occurred while retrieving teachers.",
        });
    });
};

exports.getAll = (req,res) => {
    const { page, size, groupid ,organizationid} = req.query;
    if(groupid == 'undefined' || groupid == undefined){
        res.status(400).send({ message: 'Group Id is required'});
        return true;
    }
    const { limit, offset } = getPagination(page, size);
    var condition =  { groupid : groupid ,status:1};
    Teacher.paginate(condition, { offset, limit })
        .then((data) => {
        res.send({
            totalItems: data.totalDocs,
            data: data.docs,
            totalPages: data.totalPages,
            currentPage: data.page - 1,
        });
        })
        .catch((err) => {
        res.status(500).send({
            message:
            err.message || "Some error occurred while retrieving teachers.",
        });
    });
};    

exports.register = (req, res) => {

    if(req.body.organizationid == undefined){
        res.status(400).send({ message: 'Organisation Id is required'});
        return true;
    }

    if(validator.isEmpty(req.body.organizationid)){
        res.status(400).send({ message: 'Organisation Id is required'});
        return true;
    }

    if(req.body.groupid == undefined){
        res.status(400).send({ message: 'Group ID is required'});
        return true;
    }

    if(validator.isEmpty(req.body.groupid)){
        res.status(400).send({ message: 'Group Id is required'});
        return true;
    }
    
    if(req.body.firstname == undefined){
        res.status(400).send({ message: 'Teacher first name is required'});
        return true;
    }

    if(validator.isEmpty(req.body.firstname)){
        res.status(400).send({ message: 'Teacher first name is required'});
        return true;
    }

    if(req.body.lastname == undefined){
        res.status(400).send({ message: 'Teacher last name is required'});
        return true;
    }

    if(validator.isEmpty(req.body.lastname)){
        res.status(400).send({ message: 'Teacher last name is required'});
        return true;
    }

    if(req.body.email == undefined){
        res.status(400).send({ message: 'Teacher email is required'});
        return true;
    }

    if(validator.isEmpty(req.body.email)){
        res.status(400).send({ message: 'Teacher email is required'});
        return true;
    }

    if(!validator.isEmail(req.body.email)){
        res.status(400).send({ message: 'Enter valid email'});
        return true;
    }

    if(req.body.phone == undefined){
        res.status(400).send({ message: 'Phone number is required'});
        return true;
    }

    if(validator.isEmpty(req.body.phone)){
        res.status(400).send({ message: 'Phone number is required'});
        return true;
    }

    if(!validator.isNumeric(req.body.phone)){
        res.status(400).send({ message: 'only number allowed'});
        return true;
    }

    if(req.body.phone.length < 9 ){
        res.status(400).send({ message: 'minimum 9 character allowed'});
        return true;
    }

    if(req.body.phone.length > 15 ){
        res.status(400).send({ message: 'maximum 15 character allowed'});
        return true;
    }

    if(req.body.classname == undefined){
        res.status(400).send({ message: 'Class is required'});
        return true;
    }

    if(validator.isEmpty(req.body.classname)){
        res.status(400).send({ message: 'Class is required'});
        return true;
    }

    if(req.body.subject == undefined){
        res.status(400).send({ message: 'Subject is required'});
        return true;
    }

    if(validator.isEmpty(req.body.subject)){
        res.status(400).send({ message: 'Subject is required'});
        return true;
    }

    Teacher.countDocuments({}, function(err, count) {
        if (err) throw err;
        const newId = 'TA00'+ (count + 1);
        const teacher = new Teacher({
            teacherid: newId,
            groupid: req.body.groupid,
            organizationid : req.body.organizationid,
            firstname : req.body.firstname,
            lastname : req.body.lastname,
            email: req.body.email,
            phone:req.body.phone,
            password: bcrypt.hashSync(req.body.firstname, 8),
            classname: req.body.classname,
            subject: req.body.subject,
            status :1
        });

        teacher.save((err, teacher) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }
            const user = new User({
                username: req.body.firstname,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.firstname, 8),
                //token : nodeBase64.encode("req.body.email"),
            });

            user.save((err, user) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }
                Role.findOne({ name: "teacher" }, (err, role) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }
                    user.roles = [role._id];
                    user.userType = "teacher";
                    user.token = user._id
                    user.save((err) => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }

                        //Email to user

                        sgMail.setApiKey(process.env.SENDGRID_KEY);
                        const password_token  = user._id;
                        const message = {
                            to: {
                                email: req.body.email,
                                name: req.body.firstname
                            },
                            from: {
                                email: process.env.SENDGRID_FROM_EMAIL,
                                name: process.env.SENDGRID_FROM_NAME
                            },
                            "dynamic_template_data":{
                                'user_name' : req.body.firstname,
                                'user_email' : req.body.email,
                                'password_link' : process.env.FRONEND_URL+'/set-password?token=' + password_token,
                            },
                            template_id:process.env.MEMBER_EMAIL_TEMPLATE_ID
                        };

                        sgMail.send(message).then(() => {})
                        .catch(error => {
                            console.error(error);
                        });
                        teacher.userId =  user._id;
                        teacher.save((err) => {
                            if (err) {
                                res.status(500).send({ message: err });
                                return;
                            }
                            res.send({ message: "Teacher registered successfully!" });
                        });
                    });
                });
            });
        });
    });
};


// Find a single Teacher with an id
exports.findOne = (req, res) => {
    const teacherid = req.query.id;
    if(teacherid == 'undefined' || teacherid == undefined){
        res.status(400).send({ message: 'Teacher Id is required'});
        return true;
    }
    const id = teacherid;
    Teacher.findById(id).then((data) => {
        if (!data){
            res.status(400).send({ message: "Not found Teacher with id " + id });
        } else { 
            res.send({'data':data});
        }
    })
    .catch((err) => {
        res.status(500).send({ message: "Error retrieving Teacher with id=" + id });
    });
};

// Update a Teacher by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!",
        });
    }

    if(req.body.organizationid == undefined){
        res.status(400).send({ message: 'Organisation Id is required'});
        return true;
    }

    if(validator.isEmpty(req.body.organizationid)){
        res.status(400).send({ message: 'Organisation Id is required'});
        return true;
    }

    if(req.body.groupid == undefined){
        res.status(400).send({ message: 'Group ID is required'});
        return true;
    }

    if(validator.isEmpty(req.body.groupid)){
        res.status(400).send({ message: 'Group Id is required'});
        return true;
    }
    
    if(req.body.firstname == undefined){
        res.status(400).send({ message: 'Teacher first name is required'});
        return true;
    }

    if(validator.isEmpty(req.body.firstname)){
        res.status(400).send({ message: 'Teacher first name is required'});
        return true;
    }

    if(req.body.lastname == undefined){
        res.status(400).send({ message: 'Teacher last name is required'});
        return true;
    }

    if(validator.isEmpty(req.body.lastname)){
        res.status(400).send({ message: 'Teacher last name is required'});
        return true;
    }

    if(req.body.email == undefined){
        res.status(400).send({ message: 'Teacher email is required'});
        return true;
    }

    if(validator.isEmpty(req.body.email)){
        res.status(400).send({ message: 'Teacher email is required'});
        return true;
    }

    if(!validator.isEmail(req.body.email)){
        res.status(400).send({ message: 'Enter valid email'});
        return true;
    }

    if(req.body.phone == undefined){
        res.status(400).send({ message: 'Phone number is required'});
        return true;
    }

    if(validator.isEmpty(req.body.phone)){
        res.status(400).send({ message: 'Phone number is required'});
        return true;
    }

    if(!validator.isNumeric(req.body.phone)){
        res.status(400).send({ message: 'only number allowed'});
        return true;
    }

    if(req.body.phone.length < 9 ){
        res.status(400).send({ message: 'minimum 9 character allowed'});
        return true;
    }

    if(req.body.phone.length > 15 ){
        res.status(400).send({ message: 'maximum 15 character allowed'});
        return true;
    }

    if(req.body.classname == undefined){
        res.status(400).send({ message: 'Class is required'});
        return true;
    }

    if(validator.isEmpty(req.body.classname)){
        res.status(400).send({ message: 'Class is required'});
        return true;
    }

    if(req.body.subject == undefined){
        res.status(400).send({ message: 'Subject is required'});
        return true;
    }

    if(validator.isEmpty(req.body.subject)){
        res.status(400).send({ message: 'Subject is required'});
        return true;
    }

    const id = req.body.id;
    
    //Update user collection email if requested email is different from existing record
    Teacher.findById(id).then((data) => {
        if (!data){
            res.status(400).send({ message: "Not found Teacher with id " + id });
        } else { 
            if(data.email != req.body.email){
                User.findByIdAndUpdate(data.userId, {'email': req.body.email}, { useFindAndModify: false })
                .then((data) => {
                    if (!data) {
                        res.status(404).send({ message: `Cannot update user email`,'error':err});
                    }
                });
            }
        }
    }).catch((err) => {
        res.status(500).send({message: "Error updating Teacher with id=" + id,'error':err});
    });

    const updateRecord = {
        'groupid': req.body.groupid,
        'organizationid' : req.body.organizationid,
        'firstname' : req.body.firstname,
        'lastname' : req.body.lastname,
        'email': req.body.email,
        'phone':req.body.phone,
        'classname': req.body.classname,
        'subject': req.body.subject,
    };

    Teacher.findByIdAndUpdate(id, updateRecord , { useFindAndModify: false })
    .then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot update Teacher with id= ${id}. Maybe Teacher was not found!`,'error':err});
        } else {
            res.send({ message: "Teacher was updated successfully." });
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Error updating Teacher with id=" + id,'error':err});
    });
};


//Soft delete with the specified id in the request
exports.removeData = (req, res) => {
    const id = req.body.id;
    const updateRecord = {'status': 0}
    Teacher.findByIdAndUpdate(id, updateRecord, { useFindAndModify: false })
    .then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot delete Teacher with id= ${id}. Maybe Teacher was not found!`,'error':err});
        } else {
            res.send({ message: "Teacher was deleted successfully." });
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Error deleting Teacher with id=" + id,'error':err});
    });
};
  
// Delete a Teacher with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Teacher.findByIdAndRemove(id, { useFindAndModify: false })
    .then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot delete Teacher with id=${id}. Maybe Teacher was not found!`,
        });
        } else {
            res.send({ message: "Teacher was deleted successfully!",});
        }
    })
    .catch((err) => {
        res.status(500).send({message: "Could not delete Teacher with id=" + id,'error':err });
    });
};
  
// Delete all Teacher from the database.
exports.deleteAll = (req, res) => {
    Teacher.deleteMany({})
    .then((data) => {
        res.send({ message: `${data.deletedCount} Teacher were deleted successfully!`,});
    })
    .catch((err) => {
        res.status(500).send({ message: err.message || "Some error occurred while removing all Teachers.",
        });
    });
};

// Delete all Organization  related to specific Group Admin from the database.
exports.deleteAllByGroup = (req, res) => {
    const groupId  = req.body.id;
    Teacher.deleteMany({groupid : groupId })
    .then((data) => {
        res.send({ message: `${data.deletedCount} Organization were deleted successfully!`,});
    })
    .catch((err) => {
        res.status(500).send({ message: err.message || "Some error occurred while removing all Organization.",
        });
    });
};

// Delete all Organization  related to specific Organization from the database.
exports.deleteAllByGroup = (req, res) => {
    const organizationId  = req.body.id;
    Teacher.deleteMany({organizationid : organizationId }).then((data) => {
        res.send({ message: `${data.deletedCount} Organization were deleted successfully!`,});
    })
    .catch((err) => {
        res.status(500).send({ message: err.message || "Some error occurred while removing all Organization.",
        });
    });
};

exports.getAllGroup = (req, res) => {
    Group.find({status : 1 }).sort({updatedAt : -1}).then((data) => {
        res.send({ data: data});
    })
    .catch((err) => {
        res.status(500).send({ message: err.message || "Some error occurred while fetching Group Admin.",
        });
    });
};

exports.getAllOrganization = (req, res) => {
    Organization.find({status : 1 }).sort({updatedAt : -1}).then((data) => {
        res.send({ data: data});
    })
    .catch((err) => {
        res.status(500).send({ message: err.message || "Some error occurred while fetching Organisation.",
        });
    });
};

exports.getAllOrganizationByGroup = (req, res) => {

    if(validator.isEmpty(req.body.groupid)){
        res.status(400).send({ message: 'Group Id is required'});
        return true;
    }

    const groupId  = req.body.groupid;
    Organization.find({status : 1 ,groupid : groupId}).sort({updatedAt : -1}).then((data) => {
        res.send({ data: data});
    })
    .catch((err) => {
        res.status(500).send({ message: err.message || "Some error occurred while fetching Organisation.",
        });
    });
};

exports.getList =async (req,res) =>{
    // Teacher.find({status : 1 }).sort({updatedAt : -1}).then((data) => {
    //     res.send({ data: data});
    // })
    // .catch((err) => {
    //     res.status(500).send({ message: err.message || "Some error occurred while fetching Group Admin.",
    //     });
    // });
    const { page, size } = req.query;

    try {
        let orderByField = 'teacherid'; // Field to order by
        let orderByDirection='desc'
        const { limit, offset } = getPagination(page, size);
        // Execute aggregation pipeline
        const result = await Teacher.aggregate([
            // Uncomment and define your match condition as needed, e.g., 
            { $match: {status : 1 } },
            {
              $lookup: {
                from: "groups",
                localField: "groupid",
                foreignField: "_id",
                as: "group",
              },
            },
            {
              $lookup: {
                from: "organizations",
                localField: "organizationid",
                foreignField: "_id",
                as: "organization",
              },
            },
            { $sort: { createdAt : -1 } }, // Sort by the specified field
            {
              $facet: {
                totalDocs: [{ $count: "count" }], // Get total count of documents
                docs: [
                  { $skip: offset }, // Skip records based on pagination
                  { $limit: limit }, // Limit number of records based on pagination
                  {
                    $project: {
                        id: "$_id", // Rename _id to id,
                        teacherid: 1,
                        firstname: 1,
                        lastname: 1,
                        email: 1,
                        phone: 1,
                        status: 1,
                        classname: 1,
                        subject: 1,
                        userId: 1,
                        organizationid:1,
                        groupid:1,
                        group: { $ifNull: [{ $arrayElemAt: ["$group", 0] }, {}] },
                        organization: { $ifNull: [{ $arrayElemAt: ["$organization", 0] }, {}] },
                      }
                  }
                ],
              },
            },
            {
              $project: {
                totalDocs: { $arrayElemAt: ["$totalDocs.count", 0] }, // Extract total count from 'totalDocs'
                docs: 1, // Include 'docs' array in the output
                totalPages: {
                  $ceil: { $divide: [{ $arrayElemAt: ["$totalDocs.count", 0] }, limit] }, // Calculate total pages
                },
                currentPage: { $literal: Math.floor(offset / limit) + 1 }, // Calculate current page (1-based index)
              },
            },
          ]);
        res.status(200).send({data:result[0].docs,totalDocs:result[0].totalDocs,totalPages:result[0].totalPages,currentPage:result[0].currentPage});
    } catch (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while fetching Teacher details.",
        });
    }
}

// exports.getAllTeacherByGroup = (req, res) => {

//     const page = req.body.page;
//     const size = req.body.size;
//     const groupid = req.body.groupid;

//     if(groupid == 'undefined' || groupid == undefined){
//         res.status(400).send({ message: 'Group Id is required'});
//         return true;
//     }
//     const { limit, offset } = getPagination(page, size);
//     var condition =  { groupid : groupid ,status:1};
//     Teacher.paginate(condition, { offset, limit ,sort:{ updatedAt : -1} }).then((data) => {
//         res.send({
//             totalItems: data.totalDocs,
//             data: data.docs,
//             totalPages: data.totalPages,
//             currentPage: data.page - 1,
//         });
//     })
//     .catch((err) => {
//         res.status(500).send({ message: err.message || "Some error occurred while retrieving teachers.",});
//     });
// };


exports.getAllTeacherByGroup = (req, res) => {
    const page = req.body.page;
    const size = req.body.size;
    const groupid = req.body.groupid;
  
    if (groupid == "undefined" || groupid == undefined) {
      res.status(400).send({ message: "Group Id is required" });
      return true;
    }
    const { limit, offset } = getPagination(page, size);
    var condition = {status: 1, groupid: mongoose.Types.ObjectId(groupid)};
    // const { condition, offset, limit } = req.body;
  
    Teacher.aggregate([
      { $match: condition }, // Match the documents based on the provided condition
      {
        $sort: { updatedAt: -1 }, // Sort by updatedAt in descending order
      },
      {
        $facet: {
          data: [
            { $skip: offset }, // Skip the first 'offset' documents
            { $limit: limit }, // Limit to 'limit' documents
          ],
          metadata: [
            { $count: "totalItems" }, // Count the total number of documents matching the condition
          ],
        },
      },
    ])
      .then((result) => {
        const data = result[0].data;
        const totalItems =
          result[0].metadata.length > 0 ? result[0].metadata[0].totalItems : 0;
        const totalPages = Math.ceil(totalItems / limit);
        const currentPage = offset / limit;
  
        res.send({
          totalItems,
          data,
          totalPages,
          currentPage,
        });
      })
      .catch((err) => {
        console.error(err);
        res.status(500).send({ message: "Error occurred while fetching data" });
      });
  
    // .catch((err) => {
    //     res.status(500).send({ message: err.message || "Some error occurred while retrieving teachers.",});
    // });
  };