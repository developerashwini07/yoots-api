const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Trip = db.trip;
const Vendor = db.vendor;
const Quotation = db.quotation;
const nodemailer = require('nodemailer');
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { getMaxListeners } = require("../models/user.model");
const sgMail = require('@sendgrid/mail');
const validator = require('validator');
const mongoose = require("mongoose");
const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;
    return { limit, offset };
};

exports.findAll = (req, res) => {
    const { page, size, teacherid, organizationid, groupid } = req.query;
    const condition = {};
    condition.status = 1;

    if (teacherid != undefined && teacherid != "") {
        condition.teacherid = teacherid;
    }
    if (organizationid != undefined && organizationid != '') {
        condition.organizationid = organizationid;
    }
    if (groupid != undefined && groupid != '') {
        condition.groupid = groupid;
    }
    if (req.query.filterStatus != undefined && req.query.filterStatus != '') {
        condition.tripstatus = parseInt(req.query.filterStatus);
    }
    let vendor;
    if (req.query.vendorid != undefined && req.query.vendorid != '') {
        vendor = req.query.vendorid ? req.query.vendorid : null;
    }

    console.log(vendor, "vendor")

    const { limit, offset } = getPagination(page, size);
    Trip.aggregate([
        { $match: { ...condition } },
        {
            $lookup: {
                from: "groups",
                localField: "group",
                foreignField: "_id",
                as: "group",
            },
        },
        {
            $lookup: {
                from: "teachers",
                localField: "teacher",
                foreignField: "_id",
                as: "teacher",
            },
        },
        {
            $lookup: {
                from: "organizations",
                localField: "organization",
                foreignField: "_id",
                as: "organization",
            },
        },
        {
            $lookup: {
                from: "quotations",
                localField: "_id",
                foreignField: "trip",
                as: "quotations",
            },
        },
        { $unwind: { path: "$quotations", preserveNullAndEmptyArrays: true } },
        {
            $match: vendor ? 
            {
                $and: [
                    {
                        $or: [
                            { "quotations.quotationBy": mongoose.Types.ObjectId(vendor) }, // Filter for quotations by the specified vendor
                            { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                        ]
                    },
                    {
                        $or: [
                            { "quotations.status": 1 }, // Ensure that quotations status is 1
                            { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                        ]
                    },
                    {
                        $or: [
                            { "quotations.quotationstatus": { $in: [1] } }, // Ensure that quotation status is 1
                            { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                        ]
                    },
                    {
                        $or: [
                            { "tripStatus": 1}, // Check for relevant trip statuses
                            { "tripStatus": { $exists: false } } // Handle cases where tripStatus might be missing
                        ]
                    }
                ]
            } : {
                $or: [
                    { "quotations.status": 1 },
                    { "quotations": { $eq: null } }
                ]
            }
        },
        {
            $lookup: {
                from: "vendors",
                localField: "quotations.quotationBy",
                foreignField: "_id",
                as: "quotations.vendor",
            },
        },
        {
            $unwind: { path: "$quotations.vendor", preserveNullAndEmptyArrays: true },
        },
        {
            $group: {
                _id: "$_id",
                tripDetails: { $first: "$$ROOT" },
                group: { $first: "$group" },
                teacher: { $first: "$teacher" },
                organization: { $first: "$organization" },
                quotations: { $push: "$quotations" },
            },
        },
        {
            $addFields: {
                "tripDetails.quotations": {
                    $cond: {
                        if: { $eq: ["$quotations", [{}]] },
                        then: [],
                        else: "$quotations",
                    },
                },
            },
        },
        {
            $replaceRoot: {
                newRoot: "$tripDetails",
            },
        },
        { $sort: { pickupDate: 1, tripid: -1 } }, // Sort before facet
        {
            $facet: {
                totalDocs: [{ $count: "count" }],
                docs: [{ $skip: offset }, { $limit: limit }],
            },
        },
        {
            $project: {
                totalDocs: { $ifNull: [{ $arrayElemAt: ["$totalDocs.count", 0] }, 0] },
                docs: 1,
                totalPages: {
                    $ceil: { $divide: [{ $ifNull: [{ $arrayElemAt: ["$totalDocs.count", 0] }, 0] }, limit] },
                },
                currentPage: { $literal: Math.floor(offset / limit) + 1 },
            },
        },
    ])
        .then((data) => {
            const result = data[0] || { totalDocs: 0, docs: [], totalPages: 0, currentPage: Math.floor(offset / limit) + 1 };
            res.send({
                totalItems: result.totalDocs,
                list: result.docs,
                totalPages: result.totalPages,
                currentPage: result.currentPage,
            });
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving trip.",
            });
        });
};

exports.findAllWithoutVendor = (req, res) => {
    const { page, size, teacherId, organizationId, groupId } = req.query;
    const condition = {};
    condition.status = 1;
    if (teacherId != 'undefined' && teacherId != undefined) {
        condition.teacherid = teacherId;
    }
    if (organizationId != 'undefined' && organizationId != undefined) {
        condition.organizationid = organizationId;
    }
    if (groupId != 'undefined' && groupId != undefined) {
        condition.groupid = groupId;
    }
    const { limit, offset } = getPagination(page, size);

    Trip.aggregate([
        { $match: condition },
        { $sort: { _id: -1 } },
        { $lookup: { from: 'groups', localField: 'group', foreignField: '_id', as: 'group' } },
        { $lookup: { from: 'teachers', localField: 'teacher', foreignField: '_id', as: 'teacher' } },
        { $lookup: { from: 'organizations', localField: 'organization', foreignField: '_id', as: 'organization' } },
        { $lookup: { from: 'quotations', localField: '_id', foreignField: 'trip', as: 'quotations' } },
        { $lookup: { from: 'vendors', localField: 'quotations.quotationBy', foreignField: '_id', as: 'vendor' } },
        { $skip: offset },
        { $limit: limit },
        { $facet: { totalDocs: [{ $count: "count" }], docs: [{ $skip: 0 }, { $limit: limit }] } },
        { $project: { totalDocs: { $arrayElemAt: ["$totalDocs.count", 0] }, docs: 1, totalPages: { $ceil: { $divide: [{ $arrayElemAt: ["$totalDocs.count", 0] }, limit] } }, currentPage: { $literal: Math.floor(offset / limit) } } }]).then((data) => {
            res.send({
                totalItems: data[0].totalDocs,
                list: data[0].docs,
                totalPages: data[0].totalPages,
                currentPage: data[0].currentPage,
            });
        }).catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving trip.",

            });
        });
};


exports.getAll = (req, res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    Trip.paginate({ "status": 1 }, { offset, limit }).then((data) => {
        res.send({
            totalItems: data.totalDocs,
            data: data.docs,
            totalPages: data.totalPages,
            currentPage: data.page - 1,
        });
    })
        .catch((err) => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving trip.",
            });
        });
};



exports.register = (req, res) => {

    if (req.body.tripType == undefined) {
        return res.status(400).send({ message: 'Trip Type is required' });
    }

    if (validator.isEmpty(req.body.tripType)) {
        return res.status(400).send({ message: 'Trip Type is required' });
    }

    if (req.body.adult == undefined) {
        return res.status(400).send({ message: 'Number of Adult passenger is required' });
    }

    if (validator.isEmpty(req.body.adult)) {
        return res.status(400).send({ message: 'Number of Adult passenger is required' });
    }

    if (req.body.youth == undefined) {
        return res.status(400).send({ message: 'Number of Youth passenger is required' });
    }

    if (validator.isEmpty(req.body.youth)) {
        return res.status(400).send({ message: 'Number of Youth passenger is required' });
    }

    if (req.body.pickupDate == undefined) {
        return res.status(400).send({ message: 'Pickup date is required' });
    }

    if (validator.isEmpty(req.body.pickupDate)) {
        return res.status(400).send({ message: 'Pickup date is required' });
    }

    // if(!validator.isDate(req.body.pickupDate)){
    //     res.status(400).send({ message: 'Pickup date must be date format'});
    //     return true;
    // }

    if (req.body.pickupTime == undefined) {
        return res.status(400).send({ message: 'Pickup time is required' });
    }

    if (validator.isEmpty(req.body.pickupTime)) {
        return res.status(400).send({ message: 'Pickup time is required' });
    }

    if (req.body.contactPerson == undefined) {
        return res.status(400).send({ message: 'Contact Person is required' });
    }

    if (validator.isEmpty(req.body.contactPerson)) {
        return res.status(400).send({ message: 'Contact Person is required' });
    }

    if (req.body.phone == undefined) {
        return res.status(400).send({ message: 'Contact number is required' });
    }

    if (validator.isEmpty(req.body.phone)) {
        return res.status(400).send({ message: 'Contact number is required' });
    }

    if (!validator.isNumeric(req.body.phone)) {
        return res.status(400).send({ message: 'only number allowed' });
    }

    if (req.body.phone.length < 9) {
        return res.status(400).send({ message: 'minimum 9 character allowed' });
    }

    if (req.body.phone.length > 15) {
        return res.status(400).send({ message: 'maximum 15 character allowed' });
    }

    if (req.body.vehicleType == undefined) {
        return res.status(400).send({ message: 'Vehicle type is required' });
    }

    if (validator.isEmpty(req.body.vehicleType)) {
        return res.status(400).send({ message: 'Vehicle type is required' });
    }

    if (req.body.curriculum == undefined) {
        return res.status(400).send({ message: 'Curriculum is required' });
    }

    if (validator.isEmpty(req.body.curriculum)) {
        return res.status(400).send({ message: 'Curriculum is required' });
    }

    if (req.body.curriculum !== "None") {
        if (req.body.subCategory == undefined) {
            return res.status(400).send({ message: "SubCategory is required" });
        }
        if (validator.isEmpty(req.body.subCategory)) {
            return res.status(400).send({ message: "SubCategory is required" });
        }
    }

    if (req.body.grade == undefined) {
        return res.status(400).send({ message: 'Grade is required' });
    }

    if (validator.isEmpty(req.body.grade)) {
        return res.status(400).send({ message: 'Grade is required' });
    }

    if (req.body.sourceAddress == undefined) {
        return res.status(400).send({ message: 'Pickup Location is required' });
    }

    // if(validator.isEmpty(req.body.sourceAddress)){
    //     res.status(400).send({ message: 'Pickup Location is required'});
    //     return true;
    // }

    if (req.body.destinationAddress == undefined) {
        return res.status(400).send({ message: 'Destination Location is required' });
    }

    // if(validator.isEmpty(req.body.destinationAddress)){
    //     res.status(400).send({ message: 'Destination Location is required'});
    //     return true;
    // }

    if (req.body.organizationid == undefined) {
        return res.status(400).send({ message: 'Organisation Id is required' });
    }

    if (validator.isEmpty(req.body.organizationid)) {
        return res.status(400).send({ message: 'Organisation Id is required' });
    }

    if (req.body.groupid == undefined) {
        return res.status(400).send({ message: 'Group ID is required' });
    }

    if (validator.isEmpty(req.body.groupid)) {
        return res.status(400).send({ message: 'Group Id is required' });
    }

    if (req.body.teacherid == undefined) {
        return res.status(400).send({ message: 'Teacher ID is required' });
    }

    if (validator.isEmpty(req.body.teacherid)) {
        return res.status(400).send({ message: 'Teacher Id is required' });
    }

    /*
    if (req.body.adaSeat == undefined) {
        return  res.status(400).send({ message: 'Ada Seat is required' });
    }

    if (validator.isEmpty(req.body.adaSeat)) {
        return  res.status(400).send({ message: 'Ada Seat is required' });
    }*/

    if (req.body.createdBy == undefined) {
        return res.status(400).send({ message: 'Logged in user id is required' });
    }

    if (validator.isEmpty(req.body.createdBy)) {
        return res.status(400).send({ message: 'Logged in user id is required' });
    }

    let ada_Seat = '0';
    if (req.body.adaSeat != undefined) {
        ada_Seat = req.body.adaSeat;
    }

    let return_Date = '';
    if (req.body.returnDate != undefined) {
        return_Date = req.body.returnDate;
    }

    let return_Time = '';
    if (req.body.returnTime != undefined) {
        return_Time = req.body.returnTime;
    }

    let eta_Time = '';
    if (req.body.eta != undefined) {
        eta_Time = req.body.eta;
    }



    Trip.countDocuments({}, function (err, count) {
        if (err) throw err;
        const newId = 'YT00' + (count + 1);
        const trip = new Trip({
            tripid: newId,
            teacherid: req.body.teacherid,
            teacher: req.body.teacherid,
            groupid: req.body.groupid,
            group: req.body.groupid,
            organizationid: req.body.organizationid,
            organization: req.body.organizationid,
            tripType: req.body.tripType,
            adultPassenger: req.body.adult,
            youthPassenger: req.body.youth,
            pickupDate: req.body.pickupDate,
            pickupTime: req.body.pickupTime,
            contactPerson: req.body.contactPerson,
            contactNumber: req.body.phone,
            vehicleType: req.body.vehicletype,
            curriculum: req.body.curriculum,
            subCategory: req.body.subCategory,
            grade: req.body.grade,
            adaSeat: ada_Seat,
            sourceAddress: req.body.sourceAddress,
            destinationAddress: req.body.destinationAddress,
            sourceLatlong: req.body.sourceLatlong,
            destinationLatlong: req.body.destinationLatlong,
            status: 1,
            tripstatus: 1,
            createdBy: req.body.createdBy,
            updatedBy: req.body.createdBy,
            returnDate: return_Date,
            returnTime: return_Time,
            eta: eta_Time,
            etd: req.body.etd,
        });

        trip.save((err, trip) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }

            /** Send email to vendor */
            const this_query = { status: 1 };
            Vendor.find({ this_query }, (err, docs) => {
                if (err) throw err;
                sgMail.setApiKey(process.env.SENDGRID_KEY);
                docs.forEach((doc) => {
                    //Email to user
                    const message = {
                        to: {
                            email: doc.email,
                            name: doc.vendorname
                        },
                        from: {
                            email: process.env.SENDGRID_FROM_EMAIL,
                            name: process.env.SENDGRID_FROM_NAME
                        },
                        "dynamic_template_data": {
                            'name': doc.vendorname,
                            'source': req.body.sourceAddress[0],
                            'destination': req.body.destinationAddress[0],
                            'date': req.body.pickupDate,
                            'pickup_time': req.body.pickupTime,
                            'adult': req.body.adult,
                            'youth': req.body.youth,
                            'ada_seat': req.body.adaSeat
                        },
                        template_id: process.env.TRIP_VENDOR_EMAIL_TEMPLATE_ID
                    };
                    sgMail.send(message).then(() => { })
                        .catch(error => {
                            console.error(error);
                        });
                });
            });


            Trip.findById(trip._id).populate(['group', 'teacher', 'organization']).then((this_trip) => {
                /** Send email to admin */
                User.find({}).populate('roles').exec(function (err, users) {
                    if (err) { return res.send(err); }
                    if (!users) {
                        return res.status(401).json();
                    }
                    sgMail.setApiKey(process.env.SENDGRID_KEY);
                    users.forEach((user) => {
                        if (user.roles.length > 0) {
                            if (user.roles[0].name == 'admin') {
                                var teacherName = null;
                                if (this_trip.teacher[0].firstname !== undefined) {
                                    teacherName = this_trip.teacher[0].firstname;
                                }
                                if (this_trip.teacher[0].lastname !== undefined) {
                                    teacherName = teacherName + ' ' + this_trip.teacher[0].lastname;
                                }

                                var schoolName = null;
                                if (this_trip.organization[0].organizationname !== undefined) {
                                    schoolName = this_trip.organization[0].organizationname;
                                }

                                const message = {
                                    to: {
                                        email: user.email,
                                        name: user.username
                                    },
                                    from: {
                                        email: process.env.SENDGRID_FROM_EMAIL,
                                        name: process.env.SENDGRID_FROM_NAME
                                    },
                                    "dynamic_template_data": {
                                        'name': user.username,
                                        'teacher_name': teacherName,
                                        'school_name': schoolName,
                                        'source': req.body.sourceAddress[0],
                                        'destination': req.body.destinationAddress[0],
                                        'date': req.body.pickupDate,
                                        'pickup_time': req.body.pickupTime,
                                        'adult': req.body.adult,
                                        'youth': req.body.youth,
                                        'ada_seat': req.body.adaSeat
                                    },
                                    template_id: process.env.TRIP_YOOT_EMAIL_TEMPLATE_ID
                                };
                                sgMail.send(message).then(() => { })
                                    .catch(error => {
                                        console.error(error);
                                    });
                            }
                        }
                    });
                });
            });

            res.send({ message: "Tip created successfully!", data: { id: newId } });
        });
    });
};


// Find a single Principal with an id
exports.findOne = (req, res) => {
    const tripid = req.query.id;
    if (tripid == 'undefined' || tripid == undefined) {
        res.status(400).send({ message: 'Trip Id is required' });
        return true;
    }
    const id = tripid;
    Trip.findById(id).then((data) => {
        if (!data) {
            res.status(400).send({ message: "Not found Trip with id " + id });
        } else {
            res.send({ 'data': data });
        }
    })
        .catch((err) => {
            res.status(500).send({ message: "Error retrieving Trip with id=" + id });
        });
};

// Update a Principal by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!",
        });
    }

    if (req.body.tripType == undefined) {
        res.status(400).send({ message: 'Trip Type is required' });
        return true;
    }

    if (validator.isEmpty(req.body.tripType)) {
        res.status(400).send({ message: 'Trip Type is required' });
        return true;
    }

    if (req.body.adult == undefined) {
        res.status(400).send({ message: 'Number of Adult passenger is required' });
        return true;
    }

    if (validator.isEmpty(req.body.adult)) {
        res.status(400).send({ message: 'Number of Adult passenger is required' });
        return true;
    }

    if (req.body.youth == undefined) {
        res.status(400).send({ message: 'Number of Youth passenger is required' });
        return true;
    }

    if (validator.isEmpty(req.body.youth)) {
        res.status(400).send({ message: 'Number of Youth passenger is required' });
        return true;
    }

    if (req.body.pickupDate == undefined) {
        res.status(400).send({ message: 'Pickup date is required' });
        return true;
    }

    if (validator.isEmpty(req.body.pickupDate)) {
        res.status(400).send({ message: 'Pickup date is required' });
        return true;
    }

    if (req.body.pickupTime == undefined) {
        res.status(400).send({ message: 'Pickup time is required' });
        return true;
    }

    if (validator.isEmpty(req.body.pickupTime)) {
        res.status(400).send({ message: 'Pickup time is required' });
        return true;
    }

    if (req.body.contactPerson == undefined) {
        res.status(400).send({ message: 'Contact Person is required' });
        return true;
    }

    if (validator.isEmpty(req.body.contactPerson)) {
        res.status(400).send({ message: 'Contact Person is required' });
        return true;
    }


    if (req.body.phone == undefined) {
        res.status(400).send({ message: 'Contact number is required' });
        return true;
    }

    if (validator.isEmpty(req.body.phone)) {
        res.status(400).send({ message: 'Contact number is required' });
        return true;
    }

    if (!validator.isNumeric(req.body.phone)) {
        res.status(400).send({ message: 'only number allowed' });
        return true;
    }

    if (req.body.phone.length < 9) {
        res.status(400).send({ message: 'minimum 9 character allowed' });
        return true;
    }

    if (req.body.phone.length > 15) {
        res.status(400).send({ message: 'maximum 15 character allowed' });
        return true;
    }

    if (req.body.vehicletype == undefined) {
        res.status(400).send({ message: 'Vehicle type is required' });
        return true;
    }

    if (validator.isEmpty(req.body.vehicletype)) {
        res.status(400).send({ message: 'Vehicle type is required' });
        return true;
    }

    if (req.body.curriculum == undefined) {
        res.status(400).send({ message: 'Curriculum is required' });
        return true;
    }

    if (validator.isEmpty(req.body.curriculum)) {
        res.status(400).send({ message: 'Curriculum is required' });
        return true;
    }

    if (req.body.subCategory == undefined) {
        res.status(400).send({ message: 'SubCategory is required' });
        return true;
    }

    if (validator.isEmpty(req.body.subCategory)) {
        res.status(400).send({ message: 'SubCategory is required' });
        return true;
    }

    if (req.body.grade == undefined) {
        res.status(400).send({ message: 'Grade is required' });
        return true;
    }

    if (validator.isEmpty(req.body.grade)) {
        res.status(400).send({ message: 'Grade is required' });
        return true;
    }

    if (req.body.sourceAddress == undefined) {
        res.status(400).send({ message: 'Pickup Location is required' });
        return true;
    }

    if (validator.isEmpty(req.body.sourceAddress)) {
        res.status(400).send({ message: 'Pickup Location is required' });
        return true;
    }

    if (req.body.destinationAddress == undefined) {
        res.status(400).send({ message: 'Destination Location is required' });
        return true;
    }

    if (validator.isEmpty(req.body.destinationAddress)) {
        res.status(400).send({ message: 'Destination Location is required' });
        return true;
    }

    if (req.body.organizationid == undefined) {
        res.status(400).send({ message: 'Organisation Id is required' });
        return true;
    }

    if (validator.isEmpty(req.body.organizationid)) {
        res.status(400).send({ message: 'Organisation Id is required' });
        return true;
    }

    if (req.body.groupid == undefined) {
        res.status(400).send({ message: 'Group ID is required' });
        return true;
    }

    if (validator.isEmpty(req.body.groupid)) {
        res.status(400).send({ message: 'Group Id is required' });
        return true;
    }

    if (req.body.teacherid == undefined) {
        res.status(400).send({ message: 'Teacher ID is required' });
        return true;
    }

    if (validator.isEmpty(req.body.teacherid)) {
        res.status(400).send({ message: 'Teacher Id is required' });
        return true;
    }

    if (req.body.adaSeat == undefined) {
        res.status(400).send({ message: 'Ada Seat is required' });
        return true;
    }

    if (validator.isEmpty(req.body.adaSeat)) {
        res.status(400).send({ message: 'Ada Seat is required' });
        return true;
    }

    if (req.body.updatedBy == undefined) {
        res.status(400).send({ message: 'Logged in user id is required' });
        return true;
    }

    if (validator.isEmpty(req.body.updatedBy)) {
        res.status(400).send({ message: 'Logged in user id is required' });
        return true;
    }

    const id = req.body.id;

    const updateRecord = {
        "teacherid": req.body.teacherid,
        "teacher": req.body.teacherid,
        "groupid": req.body.groupid,
        "group": req.body.groupid,
        "organizationid": req.body.organizationid,
        "organization": req.body.organizationid,
        "tripType": req.body.tripType,
        "adultPassenger": req.body.adult,
        "youthPassenger": req.body.youth,
        "pickupDate": req.body.pickupDate,
        "pickupTime": req.body.pickupTime,
        "contactPerson": req.body.contactPerson,
        "contactNumber": req.body.phone,
        "vehicleType": req.body.vehicletype,
        "curriculum": req.body.curriculum,
        "subCategory": req.body.subCategory,
        "grade": req.body.grade,
        "adaSeat": req.body.adaSeat,
        "sourceAddress": req.body.sourceAddress,
        "destinationAddress": req.body.destinationAddress,
        "updatedBy": req.body.updatedBy,
    };


    Trip.findByIdAndUpdate(id, updateRecord, { useFindAndModify: false }).then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot update trip with id= ${id}. Maybe Group was not found!`, 'error': err });
        } else {
            res.send({ message: "Trip was updated successfully." });
        }
    })
        .catch((err) => {
            res.status(500).send({ message: "Error updating trip with id=" + id, 'error': err });
        });
};


//Soft delete with the specified id in the request
exports.removeData = (req, res) => {
    const id = req.body.id;
    const updateRecord = { 'status': 0 }
    Trip.findByIdAndUpdate(id, updateRecord, { useFindAndModify: false }).then((data) => {
        if (!data) {
            res.status(404).send({ message: `Cannot delete trip with id= ${id}. Maybe trip was not found!`, 'error': err });
        } else {
            res.send({ message: "Trip was deleted successfully." });
        }
    })
        .catch((err) => {
            res.status(500).send({ message: "Error deleting Trip with id=" + id, 'error': err });
        });
};

// Delete a Principal with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Trip.findByIdAndRemove(id, { useFindAndModify: false }).then((data) => {
        if (!data) {
            res.status(404).send({
                message: `Cannot delete trip with id=${id}. Maybe trip was not found!`,
            });
        } else {
            res.send({ message: "Trip was deleted successfully!", });
        }
    })
        .catch((err) => {
            res.status(500).send({ message: "Could not delete Trip with id=" + id, 'error': err });
        });
};

// Delete all Principal from the database.
exports.deleteAll = (req, res) => {
    Trip.deleteMany({}).then((data) => {
        res.send({ message: `${data.deletedCount} Trip were deleted successfully!`, });
    })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while removing all Trip.",
            });
        });
};

exports.submitQutote = (req, res) => {
    if (req.body.tripid == undefined) {
        res.status(400).send({ message: 'Trip id is required' });
        return true;
    }
    if (validator.isEmpty(req.body.tripid)) {
        res.status(400).send({ message: 'Trip id is required' });
        return true;
    }
    if (req.body.quotationid == undefined) {
        res.status(400).send({ message: 'Quotation id is required' });
        return true;
    }
    if (validator.isEmpty(req.body.quotationid)) {
        res.status(400).send({ message: 'Quotation id is required' });
        return true;
    }
    const id = req.body.tripid;
    const quoteId = req.body.quotationid;
    Trip.findById(id).populate(['group', 'teacher', 'organization']).then((trip) => {
        if (!trip) {
            res.status(400).send({ message: "Not found Trip with id " + id });
            return true;
        } else {
            Quotation.findById(quoteId).then((quotation) => {
                if (!quotation) {
                    res.status(400).send({ message: "Not found Quotation with id " + quoteId });
                    return true;
                }
                quotation.quotationstatus = 1;
                quotation.save((err) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }
                    trip.tripstatus = 2;
                    trip.save((err) => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }
                        res.status(200).send({ 'message': 'Trip marked quoted', 'trip': trip, 'quotation': quotation });
                    });
                });
            }).catch((err) => {
                res.status(500).send({ message: "Error retrieving quotation with id=" + quoteId, 'error': err });
            });
        }
    }).catch((err) => {
        res.status(500).send({ message: "Error retrieving Trip with id=" + id, 'error': err });
    });
};

exports.confirmQutote = (req, res) => {
    if (req.body.tripid == undefined) {
        res.status(400).send({ message: 'Trip id is required' });
        return true;
    }
    if (validator.isEmpty(req.body.tripid)) {
        res.status(400).send({ message: 'Trip id is required' });
        return true;
    }
    if (req.body.quotationid == undefined) {
        res.status(400).send({ message: 'Quotation id is required' });
        return true;
    }
    if (validator.isEmpty(req.body.quotationid)) {
        res.status(400).send({ message: 'Quotation id is required' });
        return true;
    }

    if (req.body.updatedBy == undefined) {
        res.status(400).send({ message: 'updatedBy is required' });
        return true;
    }
    if (validator.isEmpty(req.body.updatedBy)) {
        res.status(400).send({ message: 'updatedBy is required' });
        return true;
    }


    const id = req.body.tripid;
    const quoteId = req.body.quotationid;
    Trip.findById(id).populate(['group', 'teacher', 'organization']).then((trip) => {
        if (!trip) {
            res.status(400).send({ message: "Not found Trip with id " + id });
            return true;
        } else {
            Quotation.findById(quoteId).then((quotation) => {
                if (!quotation) {
                    res.status(400).send({ message: "Not found Quotation with id " + quoteId });
                    return true;
                }
                quotation.quotationstatus = 1;
                quotation.save((err) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }
                    trip.tripOldStatus = trip.tripstatus;
                    trip.tripstatus = 3;
                    trip.save((err) => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }
                        res.status(200).send({ 'message': 'Trip marked confirm', 'trip': trip, 'quotation': quotation });
                    });
                });
            }).catch((err) => {
                res.status(500).send({ message: "Error retrieving quotation with id=" + quoteId, 'error': err });
            });
        }
    }).catch((err) => {
        res.status(500).send({ message: "Error retrieving Trip with id=" + id, 'error': err });
    });
};

exports.getvendorList = (req, res) => {
    const this_query = { status: 1 };
    Vendor.find({ this_query }, (err, docs) => {
        if (err) throw err;
        res.status(200).send({ data: docs });
    });
};

exports.rejectQutote = (req, res) => {
    if (req.body.tripid == undefined) {
        res.status(400).send({ message: 'Trip id is required' });
        return true;
    }
    if (validator.isEmpty(req.body.tripid)) {
        res.status(400).send({ message: 'Trip id is required' });
        return true;
    }
    if (req.body.quotationid == undefined) {
        res.status(400).send({ message: 'Quotation id is required' });
        return true;
    }
    if (validator.isEmpty(req.body.quotationid)) {
        res.status(400).send({ message: 'Quotation id is required' });
        return true;
    }

    if (req.body.updatedBy == undefined) {
        res.status(400).send({ message: 'updatedBy is required' });
        return true;
    }
    if (validator.isEmpty(req.body.updatedBy)) {
        res.status(400).send({ message: 'updatedBy is required' });
        return true;
    }

    const id = req.body.tripid;
    const quoteId = req.body.quotationid;
    Trip.findById(id).populate(['group', 'teacher', 'organization']).then((trip) => {
        if (!trip) {
            res.status(400).send({ message: "Not found Trip with id " + id });
            return true;
        } else {
            Quotation.findById(quoteId).then((quotation) => {
                if (!quotation) {
                    res.status(400).send({ message: "Not found Quotation with id " + quoteId });
                    return true;
                }
                quotation.quotationstatus = 3;
                quotation.save((err) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }
                    trip.tripstatus = 5;
                    trip.save((err) => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }
                        res.status(200).send({ 'message': 'Trip rejected', 'trip': trip, 'quotation': quotation });
                    });
                });
            }).catch((err) => {
                res.status(500).send({ message: "Error retrieving quotation with id=" + quoteId, 'error': err });
            });
        }
    }).catch((err) => {
        res.status(500).send({ message: "Error retrieving Trip with id=" + id, 'error': err });
    });
};

// Update Trip API
exports.updateTrip = async (req, res) => {
    try {
        const tripId = req.params.id;
        const updateData = req.body;

        // Find the trip by ID
        let trip = await Trip.findById(tripId);
        if (!trip) {
            return res.status(404).send({ message: "Trip not found" });
        }

        const currentDateTime = new Date();

        // Check if the last update was less than an hour ago
        // if (trip.tripUpdateDate) {
        //   const timeDifferenceInHours =
        //     (currentDateTime - trip.tripUpdateDate) / 36e5; // Convert milliseconds to hours
        //   if (timeDifferenceInHours < 1) {
        //     return res.status(400).send({
        //       message:
        //         "You can only update the trip if the last update was more than an hour ago.",
        //     });
        //   }
        // }

        // Check if the last update in the history was less than half an hour ago
        // if (trip.updateHistory.length > 0) {
        //   const lastUpdateEntry = trip.updateHistory[trip.updateHistory.length - 1];
        //   const timeDifferenceInMinutes =
        //     (currentDateTime - lastUpdateEntry.updatedAt) / 60000; // Convert milliseconds to minutes
        //   if (timeDifferenceInMinutes < 30) {
        //     return res
        //       .status(400)
        //       .send({
        //         message:
        //           "You cannot update the trip if the last update was less than half an hour ago.",
        //       });
        //   }
        // }

        // Update the trip with new data
        const updatedTripData = {
            teacherid: updateData.teacherid,
            teacher: updateData.teacherid,
            groupid: updateData.groupid,
            group: updateData.groupid,
            organizationid: updateData.organizationid,
            organization: updateData.organizationid,
            tripType: updateData.tripType,
            adultPassenger: updateData.adult,
            youthPassenger: updateData.youth,
            pickupDate: updateData.pickupDate,
            pickupTime: updateData.pickupTime,
            contactPerson: updateData.contactPerson,
            contactNumber: updateData.phone,
            vehicleType: req.body.vehicleType,
            curriculum: updateData.curriculum,
            subCategory: updateData.subCategory,
            grade: updateData.grade,
            adaSeat: updateData.adaSeat,
            sourceAddress: updateData.sourceAddress,
            destinationAddress: updateData.destinationAddress,
            sourceLatlong: updateData.sourceLatlong,
            destinationLatlong: updateData.destinationLatlong,
            status: updateData.status !== undefined ? updateData.status : trip.status,
            tripstatus:
                trip.tripstatus == 2 || trip.tripstatus == 3
                    ? 6
                    : trip.tripstatus, // Trip Edited status check
            updatedBy: updateData.updatedBy,
            returnDate: updateData.returnDate || trip.returnDate,
            returnTime: updateData.returnTime || trip.returnTime,
            tripUpdateDate: currentDateTime,
            tripOldStatus: trip.tripstatus,
            eta: updateData.eta,
            etd: updateData.etd,
        };
        // Create a shallow copy of the trip data to avoid cyclic references
        const shallowCopyTripData = { ...trip.toObject() };

        // Update the latest entry in updateHistory
        if (trip.updateHistory.length > 0) {
            trip.updateHistory[trip.updateHistory.length - 1] = {
                updatedAt: currentDateTime,
                updatedData: shallowCopyTripData,
            };
        } else {
            // If updateHistory is empty (though it shouldn't be), push the first entry
            trip.updateHistory.push({
                updatedAt: currentDateTime,
                updatedData: shallowCopyTripData,
            });
        }

        // Increment the update count
        trip.updateCount += 1;

        // Apply the update
        Object.assign(trip, updatedTripData);

        // Save the updated trip
        await trip.save();
        // // Find all quotations by tripId and update reQuoteRequired status only if it is not blank (0)
        // await Quotation.updateMany(
        //   { tripid: trip._id },
        //   { $set: { reQuoteRequired: 1} }
        // );

        res.status(200).send({
            message: "Trip update successfully!",
            data: { id: trip.tripid },
        });
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
};

// Assign The Driver Trip API
exports.assignDriver = async (req, res) => {
    try {
        const tripId = req.params.id;
        const updateData = req.body;

        // Find the trip by ID
        let trip = await Trip.findById(tripId);
        if (!trip) {
            return res.status(404).send({ message: "Trip not found" });
        }

        // Add the new driver details to the driverDetails array
        trip.driverDetails.push(updateData);

        // Save the updated trip
        await trip.save();

        res.status(200).send({
            message: "Trip updated successfully!",
            data: { id: trip.tripid },
        });
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
};

// Update tripEditAccepted status API
exports.updateTripEditAccepted = async (req, res) => {
    try {
        const tripId = req.params.id;
        const { tripEditAccepted } = req.body;

        let trip = await Trip.findById(tripId);
        if (!trip) {
            return res.status(400).send({ message: "Trip not found" });
        }

        let tripUpdate = await Trip.updateMany(
            { _id: tripId },
            {
                $set: {
                    tripEditAccepted: tripEditAccepted,
                    tripstatus: trip.tripOldStatus,
                    tripOldStatus: 0,
                }
            }
        );
        res.status(200).send({
            message: "Trip edit accepted status updated successfully!",
            data: { id: trip.tripid },
        });
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
};


// Update tripEditAccepted status API
exports.updateReQuoteRequest = async (req, res) => {
    try {
        const tripId = req.params.id;

        let trip = await Trip.findById(tripId);
        if (!trip) {
            return res.status(400).send({ message: "Trip not found" });
        }

        // // Find all quotations by tripId and update reQuoteRequired status only if it is not blank (0)
        await Quotation.updateMany(
            { tripid: trip._id, quotationstatus: 1 },
            { $set: { reQuoteRequired: 1 } }
        );
        let tripUpdate = await Trip.updateMany(
            { _id: tripId },
            { $set: { tripEditAccepted: 0 } }
        );
        res.status(200).send({
            message: "Trip re quote successfully!",
        });
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
};

