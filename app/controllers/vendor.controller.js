const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Vendor = db.vendor;
const Quotation = db.quotation;
const Trip = db.trip;
const nodemailer = require('nodemailer');
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { getMaxListeners } = require("../models/user.model");
const sgMail = require('@sendgrid/mail');
const validator = require('validator');
const mongoose = require("mongoose");
const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;
    return { limit, offset };
};

exports.findAll = (req, res) => {
    const { page, size, vendorname } = req.query;
    var condition = vendorname ? { vendorname: { $regex: new RegExp(vendorname), $options: "i" }, status: 1 } : { status: 1 };
    const { limit, offset } = getPagination(page, size);
    Vendor.paginate(condition, { offset, limit, sort: { _id: -1 } }).then((data) => {
        res.send({
            totalItems: data.totalDocs,
            data: data.docs,
            totalPages: data.totalPages,
            currentPage: data.page - 1,
        });
    })
        .catch((err) => {
            res.status(500).send({ message: err.message || "Some error occurred while retrieving vendor.", });
        });
};

exports.getAll = (req, res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    Vendor.paginate({ "status": 1 }, { offset, limit }).then((data) => {
        res.send({
            totalItems: data.totalDocs,
            data: data.docs,
            totalPages: data.totalPages,
            currentPage: data.page - 1,
        });
    })
        .catch((err) => {
            res.status(500).send({ message: err.message || "Some error occurred while retrieving vendor.", });
        });
};

exports.register = (req, res) => {

    if (req.body.vendorname == undefined) {
        res.status(400).send({ message: 'Vendor name is required' });
        return true;
    }

    if (validator.isEmpty(req.body.vendorname)) {
        res.status(400).send({ message: 'Vendor name is required' });
        return true;
    }

    if (req.body.vendoradmin == undefined) {
        res.status(400).send({ message: 'Contact person is required' });
        return true;
    }

    if (validator.isEmpty(req.body.vendoradmin)) {
        res.status(400).send({ message: 'Contact person is required' });
        return true;
    }

    if (req.body.email == undefined) {
        res.status(400).send({ message: 'Vendor email is required' });
        return true;
    }

    if (validator.isEmpty(req.body.email)) {
        res.status(400).send({ message: 'Vendor email is required' });
        return true;
    }

    if (!validator.isEmail(req.body.email)) {
        res.status(400).send({ message: 'Enter valid email' });
        return true;
    }

    if (req.body.phone == undefined) {
        res.status(400).send({ message: 'Phone number is required' });
        return true;
    }

    if (validator.isEmpty(req.body.phone)) {
        res.status(400).send({ message: 'Phone number is required' });
        return true;
    }

    if (!validator.isNumeric(req.body.phone)) {
        res.status(400).send({ message: 'only number allowed' });
        return true;
    }

    if (req.body.phone.length < 9) {
        res.status(400).send({ message: 'minimum 9 character allowed' });
        return true;
    }

    if (req.body.phone.length > 15) {
        res.status(400).send({ message: 'maximum 15 character allowed' });
        return true;
    }

    if (req.body.address == undefined) {
        res.status(400).send({ message: 'Addess is required' });
        return true;
    }

    if (validator.isEmpty(req.body.address)) {
        res.status(400).send({ message: 'Addess is required' });
        return true;
    }

    if (req.body.vehicletype == undefined) {
        res.status(400).send({ message: 'Vehicle type is required' });
        return true;
    }

    if (validator.isEmpty(req.body.vehicletype)) {
        res.status(400).send({ message: 'Vehicle type is required' });
        return true;
    }


    Vendor.countDocuments({}, function (err, count) {
        if (err) throw err;
        const newId = 'VA00' + (count + 1);
        const vendor = new Vendor({
            vendorid: newId,
            vendorname: req.body.vendorname,
            vendoradmin: req.body.vendoradmin,
            email: req.body.email,
            phone: req.body.phone,
            password: bcrypt.hashSync(req.body.vendoradmin, 8),
            address: req.body.address,
            vehicletype: req.body.vehicletype,
            status: 1
        });

        vendor.save((err, vendor) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }
            const user = new User({
                username: req.body.vendoradmin,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.vendoradmin, 8),
            });

            user.save((err, user) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }
                Role.findOne({ name: "user" }, (err, role) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }
                    user.roles = [role._id];
                    user.userType = "user";
                    user.token = user._id
                    user.save((err) => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }

                        //Email to user
                        sgMail.setApiKey(process.env.SENDGRID_KEY);
                        const password_token = user._id;
                        const message = {
                            to: {
                                email: req.body.email,
                                name: req.body.vendoradmin
                            },
                            from: {
                                email: process.env.SENDGRID_FROM_EMAIL,
                                name: process.env.SENDGRID_FROM_NAME
                            },
                            "dynamic_template_data": {
                                'user_name': req.body.vendoradmin,
                                'user_email': req.body.email,
                                'password_link': process.env.FRONEND_URL + '/set-password?token=' + password_token,
                            },
                            template_id: process.env.MEMBER_EMAIL_TEMPLATE_ID
                        };

                        sgMail.send(message).then(() => { })
                            .catch(error => {
                                console.error(error);
                            });
                        vendor.userId = user._id;
                        vendor.save((err) => {
                            if (err) {
                                res.status(500).send({ message: err });
                                return;
                            }
                            res.send({ message: "Vendor registered successfully!" });
                            return;
                        });
                    });
                });
            });
        });
    });
};


// Find a single Vendor with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Vendor.findById(id)
        .then((data) => {
            if (!data) {
                res.status(400).send({ message: "Not found Vendor with id " + id });
            } else {
                res.send({ 'data': data });
            }
        })
        .catch((err) => {
            res.status(500).send({ message: "Error retrieving Vendor with id=" + id });
        });
};

// Update a Vendor by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!",
        });
    }

    if (req.body.vendorname == undefined) {
        res.status(400).send({ message: 'Vendor name is required' });
        return true;
    }

    if (validator.isEmpty(req.body.vendorname)) {
        res.status(400).send({ message: 'Vendor name is required' });
        return true;
    }

    if (req.body.vendoradmin == undefined) {
        res.status(400).send({ message: 'Contact person is required' });
        return true;
    }

    if (validator.isEmpty(req.body.vendoradmin)) {
        res.status(400).send({ message: 'Contact person is required' });
        return true;
    }

    if (req.body.email == undefined) {
        res.status(400).send({ message: 'Group email is required' });
        return true;
    }

    if (validator.isEmpty(req.body.email)) {
        res.status(400).send({ message: 'Group email is required' });
        return true;
    }

    if (!validator.isEmail(req.body.email)) {
        res.status(400).send({ message: 'Enter valid email' });
        return true;
    }

    if (req.body.phone == undefined) {
        res.status(400).send({ message: 'Phone number is required' });
        return true;
    }

    if (validator.isEmpty(req.body.phone)) {
        res.status(400).send({ message: 'Phone number is required' });
        return true;
    }

    if (!validator.isNumeric(req.body.phone)) {
        res.status(400).send({ message: 'only number allowed' });
        return true;
    }

    if (req.body.phone.length < 9) {
        res.status(400).send({ message: 'minimum 9 character allowed' });
        return true;
    }

    if (req.body.phone.length > 15) {
        res.status(400).send({ message: 'maximum 15 character allowed' });
        return true;
    }

    if (req.body.address == undefined) {
        res.status(400).send({ message: 'Addess is required' });
        return true;
    }

    if (validator.isEmpty(req.body.address)) {
        res.status(400).send({ message: 'Addess is required' });
        return true;
    }

    if (req.body.vehicletype == undefined) {
        res.status(400).send({ message: 'Vehicle type is required' });
        return true;
    }

    if (validator.isEmpty(req.body.vehicletype)) {
        res.status(400).send({ message: 'Vehicle type is required' });
        return true;
    }

    const id = req.body.id;

    //Update user collection email if requested email is different from existing record
    Vendor.findById(id).then((data) => {
        if (!data) {
            res.status(400).send({ message: "Not found Vendor with id " + id });
        } else {
            if (data.email != req.body.email) {
                User.findByIdAndUpdate(data.userId, { 'email': req.body.email }, { useFindAndModify: false })
                    .then((data) => {
                        if (!data) {
                            res.status(404).send({ message: `Cannot update user email`, 'error': err });
                        }
                    });
            }
        }
    }).catch((err) => {
        res.status(500).send({ message: "Error updating Vendor with id=" + id, 'error': err });
    });


    const updateRecord = {
        'vendorname': req.body.vendorname,
        'vendoradmin': req.body.vendoradmin,
        'email': req.body.email,
        'phone': req.body.phone,
        'address': req.body.address,
        'vehicletype': req.body.vehicletype,
    };


    Vendor.findByIdAndUpdate(id, updateRecord, { useFindAndModify: false })
        .then((data) => {
            if (!data) {
                res.status(404).send({ message: `Cannot update Vendor with id= ${id}. Maybe Group was not found!`, 'error': err });
            } else {
                res.send({ message: "Vendor was updated successfully." });
            }
        })
        .catch((err) => {
            res.status(500).send({ message: "Error updating Vendor with id=" + id, 'error': err });
        });
};


//Soft delete with the specified id in the request
exports.removeData = (req, res) => {
    const id = req.body.id;
    const updateRecord = { 'status': 0 }
    Vendor.findByIdAndUpdate(id, updateRecord, { useFindAndModify: false })
        .then((data) => {
            if (!data) {
                res.status(404).send({ message: `Cannot delete Vendor with id= ${id}. Maybe Vendor was not found!`, 'error': err });
            } else {
                res.send({ message: "Vendor was deleted successfully." });
            }
        })
        .catch((err) => {
            res.status(500).send({ message: "Error deleting Vendor with id=" + id, 'error': err });
        });
};

// Delete a Vendor with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Vendor.findByIdAndRemove(id, { useFindAndModify: false })
        .then((data) => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete vendor with id=${id}. Maybe vendor was not found!`,
                });
            } else {
                res.send({ message: "Vendor was deleted successfully!", });
            }
        })
        .catch((err) => {
            res.status(500).send({ message: "Could not delete vendor with id=" + id, 'error': err });
        });
};

// Delete all Vendor from the database.
exports.deleteAll = (req, res) => {
    Vendor.deleteMany({}).then((data) => {
        res.send({ message: `${data.deletedCount} Vendor were deleted successfully!`, });
    })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while removing all vendor.",
            });
        });
};


exports.quoteSubmit = (req, res) => {

    if (req.body.tripid == undefined) {
        res.status(400).send({ message: 'trip id is required' });
        return true;
    }

    if (validator.isEmpty(req.body.tripid)) {
        res.status(400).send({ message: 'trip id is required' });
        return true;
    }

    if (req.body.quotationAmount == undefined) {
        res.status(400).send({ message: 'Quotation Amount is required' });
        return true;
    }

    if (validator.isEmpty(req.body.quotationAmount)) {
        res.status(400).send({ message: 'Quotation Amount is required' });
        return true;
    }

    if (req.body.quotationBy == undefined) {
        res.status(400).send({ message: 'Quotation By is required' });
        return true;
    }

    if (validator.isEmpty(req.body.quotationBy)) {
        res.status(400).send({ message: 'Quotation By is required' });
        return true;
    }

    // const this_condition = {tripid: req.body.tripid,quotationBy:req.body.quotationBy};
    // Quotation.findOne({this_condition}, function (err, result) {
    //     if (err) throw err;
    //     if(result != null){
    //         if(result.tripid != undefined || result.tripid != null ){
    //             res.status(400).send({ message: 'You have already quoated'});
    //             return;
    //         }
    //     }
    // });

    Quotation.countDocuments({}, function (err, count) {
        if (err) throw err;
        const newId = 'QT00' + (count + 1);
        const quotation = new Quotation({
            quotationid: newId,
            tripid: req.body.tripid,
            trip: req.body.tripid,
            quotationAmount: req.body.quotationAmount,
            quotationBy: req.body.quotationBy,
            status: 1,
            quotationstatus: 0,
        });

        quotation.save((err, quotation) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }
            res.send({ message: "Quotation submit successfully!" });
        });
    });
};


exports.quoteEdit = async (req, res) => {
    try {
        const Id = req.params.id;
        if (req.body.tripid == undefined) {
            res.status(400).send({ message: 'trip id is required' });
            return true;
        }

        if (validator.isEmpty(req.body.tripid)) {
            res.status(400).send({ message: 'trip id is required' });
            return true;
        }

        if (req.body.quotationAmount == undefined) {
            res.status(400).send({ message: 'Quotation Amount is required' });
            return true;
        }

        if (validator.isEmpty(req.body.quotationAmount)) {
            res.status(400).send({ message: 'Quotation Amount is required' });
            return true;
        }

        if (req.body.quotationBy == undefined) {
            res.status(400).send({ message: 'Quotation By is required' });
            return true;
        }

        if (validator.isEmpty(req.body.quotationBy)) {
            res.status(400).send({ message: 'Quotation By is required' });
            return true;
        }

        // Check if a quotation already exists for the same trip and user
        const existingQuotation = await Quotation.findById(Id);
        if (!existingQuotation) {
            return res.status(400).send({ message: 'You have already quoted for this trip' });
        }
        let trip = await Trip.findById(existingQuotation.trip);
        const currentDateTime = new Date();

        // Check if the last update was less than an hour ago
        //   if (existingQuotation.quotationUpdateDate) {
        //     const timeDifferenceInHours =
        //       (currentDateTime - existingQuotation.quotationUpdateDate) / 36e5; // Convert milliseconds to hours
        //     if (timeDifferenceInHours < 1) {
        //       return res.status(400).send({
        //         message: 'You can only update the trip if the last update was more than an hour ago.',
        //       });
        //     }
        //   }

        // Create a shallow copy of the existing quotation data to avoid cyclic references
        const shallowCopyExistingQuotation = {
            ...existingQuotation.toObject(),
        };
        const updatedTripData = {
            quotationid: req.body.quotationid,
            tripid: req.body.tripid,
            trip: req.body.tripid,
            quotationAmount: req.body.quotationAmount,
            quotationBy: req.body.quotationBy,
            status: 1,
            quotationstatus: trip.tripstatus == 1 ? 0 : req.body.quotationstatus,
            reQuoteRequired: 0,
            reQuoteDone: trip.tripstatus == 6 ? 1 : 0,
        }
        // Update the latest entry in updateHistory
        if (existingQuotation.updateHistory.length > 0) {
            existingQuotation.updateHistory[existingQuotation.updateHistory.length - 1] = {
                updatedAt: currentDateTime,
                updatedData: shallowCopyExistingQuotation,
            };
        } else {
            // If updateHistory is empty (though it shouldn't be), push the first entry
            existingQuotation.updateHistory.push({
                updatedAt: currentDateTime,
                updatedData: shallowCopyExistingQuotation,
            });
        }

        // Increment the update count
        existingQuotation.updateCount += 1;

        // Apply the update
        Object.assign(existingQuotation, updatedTripData);

        await existingQuotation.save();
        res.status(200).send({ message: 'Quotation update successfully!' });
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
};

// Update tripEditAccepted status API
exports.quoteConncel = async (req, res) => {
    try {
        const tripId = req.params.id;

        let trip = await Trip.findById(tripId);
        if (!trip) {
            return res.status(400).send({ message: "Trip not found" });
        }
        await Trip.updateMany(
            { _id: tripId },
            { $set: { tripOldStatus: 0, tripstatus: 1, updateHistory: [], updateCount: 0 } }
        );

        // // Find all quotations by tripId and update reQuoteRequired status only if it is not blank (0)
        await Quotation.updateMany(
            { tripid: trip._id },
            { $set: { status: 0, trip: null } }
        );
        res.status(200).send({
            message: "Quote cancel successfully!",
        });
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
};


exports.tripList = (req, res) => {
    const { page, size, teacherid, organizationid, groupid } = req.query;
    const condition = {};
    condition.status = 1;

    if (teacherid != undefined && teacherid != "") {
        condition.teacherid = teacherid;
    }
    if (organizationid != undefined && organizationid != "") {
        condition.organizationid = organizationid;
    }
    if (groupid != undefined && groupid != "") {
        condition.groupid = groupid;
    }
    if (req.query.filterStatus != undefined && req.query.filterStatus != "") {
        condition.tripstatus = parseInt(req.query.filterStatus);
    }
    let vendor;
    if (req.query.vendorid != undefined && req.query.vendorid != "") {
        vendor = req.query.vendorid ? req.query.vendorid : null;
    }
    const today = new Date().toISOString().slice(0, 10);
    let formatedDate = new Date(today).toISOString();
    let formatedDateRes = formatedDate.replace("Z", "+00:00");
    const condtiotnToday = formatedDateRes;

    const { limit, offset } = getPagination(page, size);
    Trip.aggregate([
        {
            $match: {
                ...condition,
                pickupDate: { $gte: new Date(condtiotnToday) },
                tripStatus: { $ne: 0 }

            }
        },
        {
            $lookup: {
                from: "groups",
                localField: "group",
                foreignField: "_id",
                as: "group",
            },
        },
        {
            $lookup: {
                from: "teachers",
                localField: "teacher",
                foreignField: "_id",
                as: "teacher",
            },
        },
        {
            $lookup: {
                from: "organizations",
                localField: "organization",
                foreignField: "_id",
                as: "organization",
            },
        },
        {
            $lookup: {
                from: "quotations",
                localField: "_id",
                foreignField: "trip",
                as: "quotations",
            },
        },
        { $unwind: { path: "$quotations", preserveNullAndEmptyArrays: true } },
        {
            $match: vendor && (req.query.filterStatus !== undefined && req.query.filterStatus !== "")
                ? {
                    $and: [
                        {
                            $or: [
                                { "quotations.quotationBy": mongoose.Types.ObjectId(vendor) }, // Filter for quotations by the specified vendor
                                { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                            ]
                        },
                        {
                            $or: [
                                { "quotations.status": 1 }, // Ensure that quotations status is 1
                                { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                            ]
                        },
                        {
                            $or: [
                                { "quotations.quotationstatus": 1 }, // Ensure that quotation status is 1
                                { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                            ]
                        },
                        {
                            $and: [
                                { "tripStatus": { $ne: 0 } }, // Exclude documents where tripStatus is 0
                                {
                                    $or: [
                                        { "tripStatus": 1 }, // Ensure that tripStatus is 1 if the field exists
                                        { "tripStatus": { $exists: false } } // Handle cases where tripStatus might be missing
                                    ]
                                }
                            ]
                        }
                    ]
                }
                : {
                    $and: [
                        {
                            $and: [
                                {
                                    // Ensure tripStatus is 1, 2, 3, or 6
                                    tripstatus: { $in: [1, 2, 3, 6] }
                                }]
                        },
                        {
                            // Match either quotations by the specified vendor or null quotations
                            $or: [
                                { "quotations.quotationBy": mongoose.Types.ObjectId(vendor) },
                                { quotations: { $eq: null } }
                            ]
                        },
                        {
                            // Ensure quotations status is 1 if quotations exist
                            $or: [
                                { "quotations.status": 1 },
                                { quotations: { $eq: null } }
                            ]
                        },
                        {
                            // Ensure that only quotations with status 0 or 1 are included if they exist
                            $or: [
                                {
                                    $and: [
                                        { "quotations.quotationBy": mongoose.Types.ObjectId(vendor) },
                                        { "quotations.quotationstatus": { $in: [0, 1] } },
                                    ]
                                },
                                { quotations: { $eq: null } }
                            ]
                        }
                    ]
                    // $or: [
                    //     {
                    //       "quotations.quotationBy": mongoose.Types.ObjectId(vendor),
                    //     }, // Filter for quotations by the specified vendor
                    //     { "quotations.status": 1 },
                    //     { quotations: { $eq: null } }, // Handle cases where quotations field might be missing
                    //     { tripStatus: 1 }, // Ensure that tripStatus is 1 if the field exists
                    //     { tripStatus: { $exists: false } }, // Handle cases where tripStatus might be missing
                    //   ],
                    // $and: [
                    // {
                    //     $or: [
                    //         { "quotations.quotationBy": mongoose.Types.ObjectId(vendor) }, // Filter for quotations by the specified vendor
                    //         { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                    //     ]
                    // },  
                    // {
                    //     $or: [
                    //         { "quotations.status": 1 }, // Ensure that quotations status is 1 if quotations exist
                    //         { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                    //     ]
                    // },
                    // {
                    //     $or: [
                    //         { "quotations.quotationstatus": { $in: [1, 0] } }, // Ensure that quotation status is 1 or 0
                    //         { "quotations": { $eq: null } } // Handle cases where quotations field might be missing
                    //     ]
                    // },
                    // {
                    //     $and: [
                    //         { "tripStatus": 1 }, // Exclude documents where tripStatus is 0
                    // {
                    //     $or: [
                    //         { "tripStatus": 1 }, // Ensure that tripStatus is 1 if the field exists
                    //         { "tripStatus": { $exists: false } } // Handle cases where tripStatus might be missing
                    //     ]
                    // }
                    // ]
                    // }
                    // ]
                }

        },
        {
            $lookup: {
                from: "vendors",
                localField: "quotations.quotationBy",
                foreignField: "_id",
                as: "quotations.vendor",
            },
        },
        {
            $unwind: { path: "$quotations.vendor", preserveNullAndEmptyArrays: true },
        },
        {
            $group: {
                _id: "$_id",
                tripDetails: { $first: "$$ROOT" },
                group: { $first: "$group" },
                teacher: { $first: "$teacher" },
                organization: { $first: "$organization" },
                quotations: { $push: "$quotations" },
            },
        },
        {
            $addFields: {
                "tripDetails.quotations": {
                    $cond: {
                        if: { $eq: ["$quotations", [{}]] },
                        then: [],
                        else: "$quotations",
                    },
                },
            },
        },
        {
            $replaceRoot: {
                newRoot: "$tripDetails",
            },
        },
        { $sort: { pickupDate: 1, tripid: -1 } }, // Sort before facet
        {
            $facet: {
                totalDocs: [{ $count: "count" }],
                docs: [{ $skip: offset }, { $limit: limit }],
            },
        },
        {
            $project: {
                totalDocs: { $ifNull: [{ $arrayElemAt: ["$totalDocs.count", 0] }, 0] },
                docs: 1,
                totalPages: {
                    $ceil: { $divide: [{ $ifNull: [{ $arrayElemAt: ["$totalDocs.count", 0] }, 0] }, limit] },
                },
                currentPage: { $literal: Math.floor(offset / limit) + 1 },
            },
        },
    ])
        .then((data) => {
            let
                result = data[0] || {
                    totalDocs: 0,
                    docs: [],
                    totalPages: 0,
                    currentPage: Math.floor(offset / limit) + 1,
                };
            res.send({
                totalItems: result.totalDocs,
                list: result.docs,
                totalPages: result.totalPages,
                currentPage: result.currentPage,
            });
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving trip.",
            });
        });
};