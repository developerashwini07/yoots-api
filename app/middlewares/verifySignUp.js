const db = require("../models");
const ROLES = db.ROLES;
const User = db.user;
const Group = db.group;

checkDuplicateUsernameOrEmail =async (req, res, next) => {
  // Username
  // User.findOne({
  //   username: req.body.username
  // }).exec((err, user) => {
  //   if (err) {
  //     res.status(500).send({ message: err });
  //     return;
  //   }

  //   if (user) {
  //     res.status(400).send({ message: "Failed! Username is already in use!" });
  //     return;
  //   }

  //   // Email
  //   User.findOne({
  //     email: req.body.email
  //   }).exec((err, user) => {
  //     if (err) {
  //       res.status(500).send({ message: err });
  //       return;
  //     }

  //     if (user) {
  //       res.status(400).send({ message: "Failed! Email is already in use!" });
  //       return;
  //     }

  //     next();
  //   });
  // });

  try {
    // Check if username exists
    const userByUsername = await User.findOne({ username: req.body.username }).exec();
    if (userByUsername) {
      return res.status(400).send({ message: "Failed! Username is already in use!" });
    }

    // Check if email exists
    const userByEmail = await User.findOne({ email: req.body.email }).exec();
    if (userByEmail) {
      return res.status(400).send({ message: "Failed! Email is already in use!" });
    }

    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

checkRolesExisted = (req, res, next) => {
  // if (req.body.roles) {
  //   for (let i = 0; i < req.body.roles.length; i++) {
  //     if (!ROLES.includes(req.body.roles[i])) {
  //       res.status(400).send({
  //         message: `Failed! Role ${req.body.roles[i]} does not exist!`
  //       });
  //       return;
  //     }
  //   }
  // }

  // next();
  try {
    const roles = req.body.roles;

    if (roles) {
      for (let i = 0; i < roles.length; i++) {
        if (!ROLES.includes(roles[i])) {
          return res.status(400).send({
            message: `Failed! Role ${roles[i]} does not exist!`
          });
        }
      }
    }

    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

checkDuplicateGroupEmail = async (req, res, next) => {
  // group email 
    // Group.findOne({email: req.body.email}).exec((err, user) => {
    //   if (err) {
    //     res.status(500).send({ message: err });
    //     return;
    //   }
    //   if (user) {
    //     res.status(400).send({ message: "Failed! email is already in use!" });
    //     return;
    //   }
    // });
    // next();
    try {
      const user = await Group.findOne({ email: req.body.email }).exec();
      if (user) {
        return res.status(400).send({ message: "Failed! Email is already in use!" });
      }
      next();
    } catch (err) {
      res.status(500).send({ message: err.message });
    }
};

checkDuplicateUserEmail =async (req, res, next) => {
  // user email 
    // User.findOne({email: req.body.email}).exec((err, user) => {
    //   if (err) {
    //     res.status(500).send({ message: err });
    //     return false;
    //   }
    //   if (user) {
    //     res.status(400).send({ message: "Failed! email is already in use!" });
    //     return false;
    //   }
    // });
    const userByEmail = await User.findOne({ email: req.body.email }).exec();
    if (userByEmail) {
      return res.status(400).send({ message: "Failed! Email is already in use!" });
    }
      next();
};


const verifySignUp = {
  checkDuplicateUsernameOrEmail,
  checkRolesExisted,
  checkDuplicateGroupEmail,
  checkDuplicateUserEmail
};



module.exports = verifySignUp;
