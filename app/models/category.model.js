module.exports = (mongoose, mongoosePaginate) => {
    var schema = mongoose.Schema(
      {
        caterogyName:String,
        parentCategory:[{ type: mongoose.Schema.Types.ObjectId,ref: "Category"}],
        status :Number,
        userId : String,
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    schema.plugin(mongoosePaginate);
    const Category = mongoose.model("Category", schema);
    return Category;
  };