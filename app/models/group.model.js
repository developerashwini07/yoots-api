/*
const mongoose = require("mongoose");

const Group = mongoose.model(
    "Group",
    new mongoose.Schema({
        groupname: String,
        groupadmin: String,
        email: String,
        phone:Number,
        password: String,
        address: String,
    })
);

module.exports = Group;
*/

module.exports = (mongoose, mongoosePaginate) => {
    var schema = mongoose.Schema(
      {
        groupid: String,
        groupname: String,
        groupadmin: String,
        email: String,
        phone:Number,
        password: String,
        address: String,
        status :Number,
        userId : String,
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    schema.plugin(mongoosePaginate);
  
    const Group = mongoose.model("Group", schema);
    return Group;
  };