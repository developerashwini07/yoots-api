module.exports = (mongoose, mongoosePaginate) => {
    var schema = mongoose.Schema(
      {
        organizationid: String,
        groupid:String,
        organizationname: String,
        organizationadmin: String,
        email: String,
        phone:Number,
        password: String,
        address: String,
        designation:String,
        status :Number,
        userId : String,
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    schema.plugin(mongoosePaginate);
    const Organization = mongoose.model("Organization", schema);
    return Organization;
  };