module.exports = (mongoose, mongoosePaginate) => {
    var schema = mongoose.Schema(
      {
        quotationid:String,
        tripid:String,
        quotationAmount:Number,
        trip:{ type: mongoose.Schema.Types.ObjectId,ref: "Trip"},
        quotationBy:{ type: mongoose.Schema.Types.ObjectId,ref: "Vendor"},
        status :Number,
        quotationstatus :Number,
        quotationUpdateDate: { type: Date, default: null }, // Initially blank
        updateHistory: [{
        updatedAt: Date,
        updatedData: mongoose.Schema.Types.Mixed // To store the entire updated document
        }],
        updateCount: { type: Number, default: 0 }, // Initially blank,
        reQuoteRequired: { type: Number, default: 0 }, // Initially blank,
        reQuoteDone: { type: Number, default: 0 }, // Initially blank,
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    schema.plugin(mongoosePaginate);
    const Quotation = mongoose.model("Quotation", schema);
    return Quotation;
};