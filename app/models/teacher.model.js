module.exports = (mongoose, mongoosePaginate) => {
    var schema = mongoose.Schema(
      {
        teacherid:String,
        organizationid: { type: mongoose.Schema.Types.ObjectId, ref: "Organization" },
        groupid: { type: mongoose.Schema.Types.ObjectId, ref: "Group" },
        firstname: String,
        lastname: String,
        email: String,
        phone:Number,
        password: String,
        status :Number,
        classname:String,
        subject:String,
        userId : String,
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    schema.plugin(mongoosePaginate);
    const Teacher = mongoose.model("Teacher", schema);
    return Teacher;
  };