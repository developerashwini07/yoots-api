module.exports = (mongoose, mongoosePaginate) => {
    var schema = mongoose.Schema(
      {
        tripid:String,
        tripType:String,
        adultPassenger:Number,
        youthPassenger:Number,
        pickupDate:Date,
        pickupTime:String,
        contactPerson:String,
        contactNumber:String,
        vehicleType:String,
        curriculum:String,
        subCategory:String,
        grade:String,
        adaSeat:String,
        sourceAddress: Array,
        sourceLatlong: {type:Array, default:null},
        destinationAddress: Array,
        destinationLatlong: {type:Array, default:null},
        organizationid: String,
        groupid:String,
        teacherid:String,
        organization:[{ type: mongoose.Schema.Types.ObjectId,ref: "Organization"}],
        group:[{ type: mongoose.Schema.Types.ObjectId,ref: "Group"}],
        teacher:[{ type: mongoose.Schema.Types.ObjectId,ref: "Teacher"}],
        createdBy:{ type: mongoose.Schema.Types.ObjectId,ref: "User"},
        updatedBy:{ type: mongoose.Schema.Types.ObjectId,ref: "User"},
        returnDate:Date,
        returnTime:String,
        status :Number,
        tripstatus :Number,
        eta:String ,
        etd:String ,
        tripUpdateDate: { type: Date, default: null }, // Initially blank
        updateHistory: [{ 
          updatedAt: Date,
          updatedData: mongoose.Schema.Types.Mixed // To store the entire updated document
        }],
        updateCount: { type: Number, default: 0 }, // Initially blank,
        driverDetails: [{
          type :mongoose.Schema.Types.Mixed, // To store the entire updated document
          // createdAt: Date,
        }],
        tripEditAccepted: { type: Number, default: 0 }, // Initially set to 0
        tripOldStatus: { type: Number, default: 0 }, // Initially set to 0
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    schema.plugin(mongoosePaginate);
    const Trip = mongoose.model("Trip", schema);
    return Trip;
};