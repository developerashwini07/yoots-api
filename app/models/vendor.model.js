module.exports = (mongoose, mongoosePaginate) => {
    var schema = mongoose.Schema(
      {
        vendorid: String,
        vendorname: String,
        vendoradmin: String,
        email: String,
        phone:Number,
        password: String,
        address: String,
        vehicletype:String,
        status :Number,
        userId : String,
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    schema.plugin(mongoosePaginate);
    const Vendor = mongoose.model("Vendor", schema);
    return Vendor;
  };