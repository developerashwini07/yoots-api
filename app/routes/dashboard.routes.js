const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/dashboard.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        next();
    });

    app.get("/api/dashboard/yoots", controller.yoot);
    app.get("/api/dashboard/group", controller.group);
    app.get("/api/dashboard/organization", controller.organization);
    app.get("/api/dashboard/teacher", controller.teacher);
    app.get("/api/dashboard/vendor", controller.vendor);
};
