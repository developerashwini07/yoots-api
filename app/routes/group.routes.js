const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/group.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        next();
    });

    app.post(
        "/api/group/signup",
        [
            verifySignUp.checkDuplicateUserEmail,
            verifySignUp.checkDuplicateGroupEmail,        
        ],
        controller.register
    );

    app.post("/api/group/delete", controller.removeData);
    app.post("/api/group/update", controller.update);

    app.get("/api/group/find-all", controller.findAll);
    app.get("/api/group/get-all", controller.getAll);

    app.get("/api/group/developer-delete", controller.delete);
    app.get("/api/group/developer-delete-all", controller.deleteAll);
};
