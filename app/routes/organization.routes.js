const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/organization.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        next();
    });

    app.post(
        "/api/organization/signup",
        [
            verifySignUp.checkDuplicateUserEmail,
            verifySignUp.checkDuplicateGroupEmail,        
        ],
        controller.register
    );

    app.post("/api/organization/delete", controller.removeData);
    app.post("/api/organization/update", controller.update);

    app.get("/api/organization/find-all", controller.findAll);
    app.get("/api/organization/get-all", controller.getAll);

    app.get("/api/organization/group-all", controller.getAllGroup);

    app.get("/api/organization/developer-delete", controller.delete);
    app.get("/api/organization/developer-delete-all", controller.deleteAll);
};
