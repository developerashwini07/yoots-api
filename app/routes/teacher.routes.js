const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/teacher.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        next();
    });

    app.post(
        "/api/teacher/signup",
        [
            verifySignUp.checkDuplicateUserEmail,
            verifySignUp.checkDuplicateGroupEmail,        
        ],
        controller.register
    );

    app.post("/api/teacher/delete", controller.removeData);
    app.post("/api/teacher/update", controller.update);
    app.get("/api/teacher/find-all", controller.findAll);
    app.get("/api/teacher/get-all", controller.getAll);
    app.get("/api/teacher/group-all", controller.getAllGroup);
    app.get("/api/teacher/organization-all", controller.getAllOrganization);
    app.post("/api/teacher/organization-group-all", controller.getAllOrganizationByGroup);
    app.post("/api/teacher/get-teacher-groupid", controller.getAllTeacherByGroup);
    app.get("/api/teacher/get-list", controller.getList);
    app.get("/api/teacher/developer-delete", controller.delete);
    app.get("/api/teacher/developer-delete-all", controller.deleteAll);
    app.get("/api/teacher/detail", controller.findOne);
};
