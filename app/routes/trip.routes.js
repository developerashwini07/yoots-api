const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/trip.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        next();
    });

    app.post("/api/trip/create",controller.register);
    app.post("/api/trip/delete", controller.removeData);
    app.post("/api/trip/update", controller.update);
    app.get("/api/trip/find-all", controller.findAll);
    app.get("/api/trip/get-all", controller.getAll);
    app.get("/api/trip/developer-delete", controller.delete);
    app.get("/api/trip/developer-delete-all", controller.deleteAll);
    app.get("/api/trip/detail", controller.findOne);
    app.post("/api/trip/quotation-submit", controller.submitQutote);
    app.post("/api/trip/quotation-confirm", controller.confirmQutote);
    app.get("/api/trip/vendor-list", controller.getvendorList);
    app.get("/api/trip/trip-list", controller.findAllWithoutVendor);
    app.post("/api/trip/quotation-reject", controller.rejectQutote);
    app.put("/api/trip/edit/:id", controller.updateTrip);
    app.put("/api/trip/assign/:id", controller.assignDriver);
    app.put('/api/trip/edit-accepted/:id', controller.updateTripEditAccepted);
    app.put('/api/trip/re-quote/:id', controller.updateReQuoteRequest);


};
