const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/vendor.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        next();
    });

    app.post("/api/vendor/signup",
        [
            verifySignUp.checkDuplicateUserEmail,
            verifySignUp.checkDuplicateGroupEmail,        
        ],
        controller.register
    );

    app.post("/api/vendor/delete", controller.removeData);
    app.post("/api/vendor/update", controller.update);

    app.get("/api/vendor/find-all", controller.findAll);
    app.get("/api/vendor/get-all", controller.getAll);

    app.get("/api/vendor/developer-delete", controller.delete);
    app.get("/api/vendor/developer-delete-all", controller.deleteAll);

    app.post("/api/vendor/quote", controller.quoteSubmit);
    app.put("/api/vendor/quote/edit/:id", controller.quoteEdit);
    app.put("/api/vendor/quote/cancel/:id", controller.quoteConncel);
    app.get("/api/vendor/trip", controller.tripList);

};
